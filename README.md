Fin-finance
===========

Requirements:

- Php 7.1
- MySQL
- NodeJS, NPM and Bower
- Compass and Ruby (>=2.3)
- Composer

Install run:

    composer install
    bower install
    bin/console assets:install
    bin/console assetic:dump
    bin/console doctrine:database:create
    bin/console doctrine:schema:create

To start server run:

    bin/console server:run

For database use: src/FinanceBundle/Resources/dumps/PagesAndPagesTranslationDump20170109-2.sql 

To create admin user - register any user and then update his `roles` (in mysql/MySQL Workbench) field with `a:1:{i:0;s:10:"ROLE_ADMIN";}`
