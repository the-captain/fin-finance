<?php

namespace FinanceBundle\Controller;

use Doctrine\ORM\EntityManager;
use FinanceBundle\Entity\Order;
use FinanceBundle\Service\CartItemsService;
use FinanceBundle\Service\OrderService;
use Monolog\Logger;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\FlowConfig;
use PayPal\Api\InputFields;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\Presentation;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Exception\PayPalConnectionException;
use PayPal\Rest\ApiContext;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class PayPalController extends PaymentController
{
    /**
     * @var ApiContext
     */
    private $apiContext;

    /**
     * @var FormFactory
     */
    private $formFactory;

    /**
     * @var OrderService
     */
    private $orderService;

    /**
     * @var array
     */
    private $locale = [
        'en' => 'US',
        'de' => 'DE',
        'es' => 'ES',
        'ru' => 'RU'
    ];

    /**
     * @param CartItemsService $cartItems
     * @param Router           $clientId
     * @param EntityManager    $clientSecret
     * @param EntityManager    $entityManager
     * @param Router           $router
     * @param FormFactory      $formFactory
     * @param OrderService     $orderService
     * @param Logger           $logger
     */
    public function __construct(
        CartItemsService $cartItems,
        $clientId,
        $clientSecret,
        EntityManager $entityManager,
        Router $router,
        FormFactory $formFactory,
        OrderService $orderService,
        Logger $logger
    ) {
        parent::__construct($cartItems, $router, $entityManager, $logger);

        $this->formFactory = $formFactory;
        $this->orderService = $orderService;
        $this->apiContext = new ApiContext(
            new OAuthTokenCredential(
                $clientId,
                $clientSecret
            )
        );
        $this->apiContext->setConfig(
            array(
                'mode' => 'live'
            )
        );
    }

    /**
     * @return JsonResponse
     */
    public function preparePaypalExpressCheckoutPaymentAction(Request $request)
    {
        $data = $this->cartItems->getItemsFromSession();

        if (empty($data['items'])) {
            throw new AccessDeniedException();
        }

        $order = new Order();
        $form = $this->formFactory->create('FinanceBundle\Form\Type\OrderType', $order);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $order = $this->orderService->createOrder($data['items'], $order);
            $request->getSession()->set('orderId', $order->getId());
        } else {
            throw new AccessDeniedException();
        }

        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        $items = [];
        $total = 0;
        foreach ($data['items'] as $orderItem) {
            $item = new Item();
            $item->setName($orderItem->getName())
                ->setCurrency(self::CURRENCY)
                ->setQuantity(1)
                ->setPrice($orderItem->getCost());
            $items[] = $item;
            $total += $orderItem->getCost();
        }

        $itemList = new ItemList();
        $itemList->setItems($items);

        $transaction = new Transaction();
        $transaction->setAmount($this->getAmount($total))
            ->setItemList($itemList)
            ->setDescription('FinFinance consultation')
            ->setInvoiceNumber(uniqid());

        $payment = new Payment();
        $payment->setIntent('sale')
            ->setExperienceProfileId(
                $this->getFlowConfig($request->getLocale())->getId()
            )
            ->setPayer($payer)
            ->setTransactions(array($transaction))
            ->setRedirectUrls($this->getRedirectUrls());

        try {
            $payment->create($this->apiContext);
            $order->setTransactionId($payment->getId());
            $this->save($order);
        } catch (\Exception $ex) {
            $this->logger->warning('Paypal payment creation error', [$ex]);

            return new JsonResponse(['error' => 'Paypal payment creation error']);
        }

        $this->logger->addInfo('Paypal payment created', [$order, $payment]);

        return new JsonResponse(['token' => $payment->getId()]);
    }

    /**
     * @param int $total
     *
     * @return Amount
     */
    private function getAmount($total)
    {
        $details = new Details();
        $details->setSubtotal($total);

        $amount = new Amount();
        $amount->setCurrency(self::CURRENCY)
            ->setTotal($total)
            ->setDetails($details);

        return $amount;
    }

    /**
     * @param $locale
     *
     * @return bool|\PayPal\Api\CreateProfileResponse
     */
    private function getFlowConfig($locale)
    {
        $flowConfig = new FlowConfig();
        $flowConfig->setLandingPageType('Login');
        $flowConfig->setUserAction('commit');
        $flowConfig->setReturnUriHttpMethod('POST');

        $presentation = new Presentation();

        $presentation->setBrandName('FinFinance')
            ->setLocaleCode($this->locale[$locale])
            ->setReturnUrlLabel('Return')
            ->setNoteToSellerLabel('Thanks!');

        $inputFields = new InputFields();

        $inputFields->setAllowNote(true)
            ->setNoShipping(1)
            ->setAddressOverride(0);

        $webProfile = new \PayPal\Api\WebProfile();
        $webProfile->setName('FinFinance_'.uniqid())
            ->setFlowConfig($flowConfig)
            ->setPresentation($presentation)
            ->setInputFields($inputFields)
            ->setTemporary(true);
        try {
            return $webProfile->create($this->apiContext);
        } catch (PayPalConnectionException $ex) {
            $this->logger->warning('PayPalConnectionException at FlowConfig', [$ex->getMessage()]);

            return false;
        }
    }

    /**
     * @return RedirectUrls
     */
    private function getRedirectUrls()
    {
        $redirectUrls = new RedirectUrls();
        $redirectUrls->setCancelUrl(
            $this->router->generate('finance_cart_payment_cancel', [], 0)
        )->setReturnUrl(
            $this->router->generate('finance_cart_complited', [], 0)
        );

        return $redirectUrls;
    }
}
