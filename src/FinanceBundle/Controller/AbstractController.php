<?php

namespace FinanceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller as BaseSymfonyController;

abstract class AbstractController extends BaseSymfonyController
{
    /**
     * @param string $entity
     *
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    protected function getRep($entity)
    {
        return $this->getManager()->getRepository($entity);
    }

    /**
     * @return \Doctrine\Common\Persistence\ObjectManager|object
     */
    protected function getManager()
    {
        return $this->getDoctrine()->getManager();
    }

    /**
     * @param $entity
     */
    protected function save($entity)
    {
        $em = $this->getManager();
        $em->persist($entity);
        $em->flush();
    }

    /**
     * @param $entity
     */
    protected function delete($entity)
    {
        $em = $this->getManager();
        $em->remove($entity);
        $em->flush();
    }
}
