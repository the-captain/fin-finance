<?php

namespace FinanceBundle\Controller;

use Doctrine\ORM\EntityManager;
use FinanceBundle\Service\CartItemsService;
use Monolog\Logger;
use Symfony\Bundle\FrameworkBundle\Routing\Router;

abstract class PaymentController
{
    protected const CURRENCY = 'EUR';

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var CartItemsService
     */
    protected $cartItems;

    /**
     * @var Router
     */
    protected $router;

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * PaymentController constructor.
     *
     * @param CartItemsService $cartItems
     * @param Router           $router
     * @param Logger           $logger
     */
    public function __construct(
        CartItemsService $cartItems,
        Router $router,
        EntityManager $entityManager,
        Logger $logger
    ) {
        $this->logger = $logger;
        $this->cartItems = $cartItems;
        $this->router = $router;
        $this->entityManager = $entityManager;
    }

    /**
     * @param string $entity
     *
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    protected function getRep($entity)
    {
        return $this->getManager()->getRepository($entity);
    }

    /**
     * @return \Doctrine\Common\Persistence\ObjectManager|object
     */
    protected function getManager()
    {
        return $this->entityManager;
    }

    /**
     * @param $entity
     */
    protected function save($entity)
    {
        $em = $this->getManager();
        $em->persist($entity);
        $em->flush();
    }
}
