<?php

namespace FinanceBundle\Controller;

use FinanceBundle\Entity\Order;
use FinanceBundle\Form\Type\UserEditType;
use Symfony\Component\HttpFoundation\Request;

class UserAccountController extends AbstractController
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function accountAction(Request $request)
    {
        $showSuccess = false;
        if (
            $request->getSession()->has('showSuccess')
            && $request->getSession()->get('showSuccess')
        ) {
            $request->getSession()->remove('showSuccess');
            $showSuccess = true;
        }

        return $this->render(
            'FinanceBundle:UserAccount:account.html.twig',
            [
                'showSuccess' => $showSuccess
            ]
        );
    }

    /**
     * @param $request Request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request)
    {
        $editForm = $this->createForm(UserEditType::class, $this->getUser());

        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->save($this->getUser());

            return $this->redirectToRoute('finance_user_account');
        }

        return $this->render(
            'FinanceBundle:UserAccount:edit.html.twig',
            [
                'edit_form' => $editForm->createView(),
            ]
        );
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function ordersAction()
    {
        $orders = $this->getRep('FinanceBundle:Order')->findBy(
            [
                'status' => [Order::NEW_ORDER, Order::IN_PROCESS, Order::PAYED],
                'user' => $this->getUser()
            ]
        );

        return $this->render(
            'FinanceBundle:UserAccount:orders.html.twig',
            [
                'orders' => $orders,
            ]
        );
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function historyAction()
    {
        $orders = $this->getRep('FinanceBundle:Order')->findBy(
            [
                'status' => [Order::PAYED, Order::DONE],
                'user' => $this->getUser()
            ]
        );

        return $this->render(
            'FinanceBundle:UserAccount:history.html.twig',
            [
                'orders' => $orders,
            ]
        );
    }
}
