<?php

namespace FinanceBundle\Controller;

use Symfony\Component\HttpFoundation\Request;

class ContactController extends AbstractController
{
    private const PAGE_CONTACTS_ID = 10;

    private const MAIL_SUBJECT = 'Новое сообщение из формы контактов';

    /**
     * @var array
     */
    private $themeToEmail = [
        'PSP' => 1,
        'Готовые компании' => 2,
        'Банковский счет' => 3,
        'Мерчант аккаунт' => 4,
        'Другое' => 5,
    ];

    /**
     * @param $request Request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexPageAction(Request $request)
    {
        $messageSent = false;
        $page = $this->getRep('FinanceBundle:Page')->findOneById(self::PAGE_CONTACTS_ID);
        if (!$page) {
            throw $this->createNotFoundException();
        }

        $form = $this->createForm('FinanceBundle\Form\Type\ContactType');
        $clonedForm = clone $form;
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $this->get('finance.cleantalk')->checkContactRequest($form, $request);
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $mailer = $this->get('finance.mailer');
            $mailer->sendTemplate(
                '@Finance/Emails/contactRequest/index.mjml.twig',
                [
                    'fullName' => $form->getData()['name'],
                    'email' => $form->getData()['email'],
                    'phone' => $form->getData()['phone'],
                    'theme' => $form->getData()['theme'],
                    'text' => $form->getData()['text'],
                    'ip' => $request->getClientIp(),
                    'userLanguage' => $request->getLocale(),
                ],
                self::MAIL_SUBJECT,
                $this->getTargetEmailByTheme($form->getData()['theme'])
            );

            if ($form->getData()['email']) {
                $mailer->sendTemplate(
                    '@Finance/Emails/contactRequest/user-message.mjml.twig',
                    [],
                    $this->get('translator')->trans('Ваш запрос находится в обработке'),
                    $form->getData()['email']
                );
            }

            $messageSent = true;
            $form = $clonedForm;
        }

        $formErrors = $form->getErrors(true);

        return $this->render(
            '@Finance/Contact/indexPage.html.twig',
            [
                'form' => $form->createView(),
                'form_errors' => $formErrors,
                'page' => $page,
                'messageSent' => $messageSent,
            ]
        );
    }

    /**
     * @param $themeId
     *
     * @return string
     */
    private function getTargetEmailByTheme($themeId)
    {
        return $this->getRep('FinanceBundle:Config')->findOneById(
            $this->themeToEmail[$themeId]
        )->getValue();
    }
}
