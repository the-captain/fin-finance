<?php

namespace FinanceBundle\Controller;

use FinanceBundle\Entity\Item;

class DefaultController extends AbstractController
{
    public function indexAction()
    {
        $merchant = $this->getRep('FinanceBundle:Item')->findMinCostByType(Item::TYPE_MERCHANT);
        $company = $this->getRep('FinanceBundle:Item')->findMinCostByType(Item::TYPE_COMPANY);
        $bank = $this->getRep('FinanceBundle:Item')->findMinCostByType(Item::TYPE_BANK);
        $special = $this->getRep('FinanceBundle:Item')->findMinCostByType(Item::TYPE_SPECIAL_OFFERS);

        return $this->render(
            'FinanceBundle:Default:index.html.twig',
            [
                'merchant' => $merchant,
                'company' => $company,
                'special' => $special,
                'bank' => $bank,
            ]
        );
    }
}
