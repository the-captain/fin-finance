<?php

namespace FinanceBundle\Controller;

use FinanceBundle\Entity\User;
use FinanceBundle\Form\Type\UserChangePasswordType;
use FinanceBundle\Form\Type\UserResetPasswordType;
use FinanceBundle\Form\Type\UserType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class SecurityController extends AbstractController
{
    private const EMAIL_SUBJ = 'Регистрация завершена!';

    private const CONFIRM_EMAIL_SUBJ = 'Подтверждение регистрации';

    /**
     * Login Page.
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function loginAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $this->get('finance.cleantalk')->checkUser($form, $request);
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $password = $this->get('security.password_encoder')
                ->encodePassword($user, $form->get('password')->getData());

            $hash = uniqid('finconfirm', true);

            $user->setRoles(['ROLE_USER'])
            ->setPassword($password)
            ->setConfirmationHash($hash);

            $this->authenticateUser($user);
            $this->save($user);
            $this->sendConfirmEmail($user);

            return $this->redirectToRoute('finance_user_account');
        }

        $request->getSession()->set('ct_submit_time', time());
        $authenticationUtils = $this->get('security.authentication_utils');
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render(
            'FinanceBundle:Security:login.html.twig',
            [
                'last_username' => $lastUsername,
                'error' => $error,
                'form' => $form->createView(),
                'user_form_errors' => $form->getErrors(true)
            ]
        );
    }

    /**
     * @param Request $request
     * @param $hash
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function confirmAction(Request $request, $hash)
    {
        /** @var User $user */
        $user = $this->getRep('FinanceBundle:User')->findOneBy(
            [
                'id' => $this->getUser()->getId(),
                'confirmationHash' => $hash,
            ]
        );

        if (!$user) {
            throw $this->createNotFoundException();
        }

        $user->setConfirmationHash('')
            ->setConfirmed(true);
        $this->sendRegEmail($user);
        $this->save($user);
        $request->getSession()->set('showSuccess', true);

        return $this->redirectToRoute('finance_user_account');
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function resendConfirmAction()
    {
        $user = $this->getUser();
        if (!$user->getConfirmationHash()) {
            $hash = uniqid('finconfirm', true);
            $user->setConfirmationHash($hash);
            $this->save($user);
        }

        $this->sendConfirmEmail($user);
        return $this->redirectToRoute('finance_user_account');
    }

    /**
     * @param User $user
     */
    private function authenticateUser(User $user)
    {
        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());

        $this->get('security.token_storage')->setToken($token);
    }

    /**
     * @param User $user
     *
     * @return \stdClass
     */
    private function sendConfirmEmail(User $user)
    {
        return $this->get('finance.mailer')->sendTemplate(
            '@Finance/Emails/confirmation/index.mjml.twig',
            [
                'hash' => $user->getConfirmationHash(),
            ],
            $this->get('translator')->trans(self::CONFIRM_EMAIL_SUBJ),
            $user->getEmail()
        );
    }

    /**
     * @param User $user
     *
     * @return \stdClass
     */
    private function sendRegEmail(User $user)
    {
        return $this->get('finance.mailer')->sendTemplate(
            '@Finance/Emails/registration/index.mjml.twig',
            [
                'firstName' => $user->getFirstName(),
                'lastName' => $user->getLastName(),
                'phone' => $user->getPhone(),
                'email' => $user->getEmail(),
            ],
            $this->get('translator')->trans(self::EMAIL_SUBJ),
            $user->getEmail()
        );
    }

    /**
     * This controller will not be executed, as the route is handled by the Security system.
     */
    public function loginCheckAction()
    {
    }

    /**
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function recoverPasswordAction(Request $request)
    {
        $form = $this->createForm('FinanceBundle\Form\Type\UserRecoveryType');
        $clonedForm = clone $form;
        $form->handleRequest($request);
        $messageSent = false;

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var User $user */
            $user = $this->getRep('FinanceBundle:User')->findOneByEmail($form->getData()['email']);

            if (!$user) {
                $form->addError(new FormError($this->get('translator')->trans('User not found')));
            } else {
                $hash = uniqid('finreset', true);
                $user->setResetHash($hash);
                $this->save($user);
                $this->get('finance.mailer')->sendTemplate(
                    '@Finance/Emails/changePassword/index.mjml.twig',
                    [
                        'hash' => $hash,
                    ],
                    $this->get('translator')->trans('Изменить пароль'),
                    $user->getEmail()
                );

                $form = $clonedForm;
                $messageSent = true;
            }
        }

        return $this->render(
            'FinanceBundle:Security:recovery.html.twig',
            [
                'form' => $form->createView(),
                'errors' => $form->getErrors(true),
                'messageSent' => $messageSent,
            ]
        );
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function resetPasswordAction(Request $request, $hash)
    {
        /** @var User $user */
        $user = $this->getRep('FinanceBundle:User')->findOneByResetHash($hash);

        if (!$user) {
            throw $this->createNotFoundException();
        }

        $editForm = $this->createForm(UserResetPasswordType::class, $user);

        $editForm->handleRequest($request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $user->setResetHash('');

            return $this->changePassword($user, $editForm);
        }

        return $this->render(
            'FinanceBundle:UserAccount:reset-password.html.twig',
            [
                'edit_form' => $editForm->createView(),
                'form_errors' => $editForm->getErrors(true),
            ]
        );
    }

    /**
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function changePasswordAction(Request $request)
    {
        $editForm = $this->createForm(UserChangePasswordType::class, $this->getUser());

        $editForm->handleRequest($request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            return $this->changePassword($this->getUser(), $editForm);
        }

        return $this->render(
            'FinanceBundle:UserAccount:change-password.html.twig',
            [
                'edit_form' => $editForm->createView(),
                'form_errors' => $editForm->getErrors(true),
            ]
        );
    }

    /**
     * @param $user
     * @param Form $form
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    private function changePassword(User $user, Form $form)
    {
        $encoder = $this->get('security.password_encoder');
        $password = $encoder->encodePassword($user, $form->get('new_password')->getData());
        $user->setPassword($password);
        $this->save($user);
        $this->authenticateUser($user);

        return $this->redirectToRoute('finance_user_account');
    }
}
