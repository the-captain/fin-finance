<?php

namespace FinanceBundle\Controller;

use Doctrine\ORM\EntityManager;
use FinanceBundle\Entity\Item;
use FinanceBundle\Service\CartService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CartController
{
    /**
     * @var CartService
     */
    private $cart;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @param CartService   $cart
     * @param EntityManager $entityManager
     */
    public function __construct(
        CartService $cart,
        EntityManager $entityManager
    ) {
        $this->cart = $cart;
        $this->entityManager = $entityManager;
    }

    /**
     * @param $itemId
     *
     * @return JsonResponse
     */
    public function addToCartAction($itemId)
    {
        $this->cart->addItem($this->getItem($itemId)->getId());

        return $this->generateResponse();
    }

    /**
     * @param $itemId
     *
     * @return Item
     */
    private function getItem($itemId)
    {
        $item = $this->entityManager->getRepository('FinanceBundle:Item')->findOneById($itemId);

        if (!$item) {
            throw new NotFoundHttpException(sprintf('Item %s not found', $itemId));
        }

        return $item;
    }

    /**
     * @param bool   $value
     * @param string $key
     *
     * @return JsonResponse
     */
    private function generateResponse($value = true, $key = 'added')
    {
        $response = new JsonResponse([$key => $value]);
        $response->headers->setCookie($this->cart->generateCookies());

        return $response;
    }

    /**
     * @param Request $request
     * @param $itemId
     *
     * @return JsonResponse
     */
    public function addToCartSpecialAction(Request $request, $itemId)
    {
        $parentItem = $this->getItem($itemId);

        if ($parentItem->getType() !== Item::TYPE_SPECIAL_OFFERS || !$request->request->has('ids')) {
            throw new NotFoundHttpException(
                sprintf('Item %s doesn\'t have childs, or ids are missed', $itemId)
            );
        }

        $parentCount = $this->cart->addItem($parentItem->getId());

        foreach ($request->request->get('ids') as $id) {
            $item = $this->getItem($id);
            $this->cart->addChildItem($parentItem->getId(), $item->getId(), $parentCount);
        }

        return $this->generateResponse();
    }

    /**
     * @param $itemId
     * @param $type
     *
     * @return JsonResponse
     */
    public function addToCartBankAction($itemId, $type)
    {
        $this->cart->addBankItem($this->getItem($itemId)->getId(), $type);

        return $this->generateResponse();
    }

    /**
     * @param $itemId
     *
     * @return JsonResponse
     */
    public function deleteFromCartAction($itemId)
    {
        return $this->generateResponse($this->cart->deleteItem($itemId), 'deleted');
    }

    /**
     * @return JsonResponse
     */
    public function addArrayToCartAction(Request $request)
    {
        if ($request->request->has('ids')) {
            foreach ($request->request->get('ids') as $id) {
                $this->cart->addItem($this->getItem($id)->getId());
            }

            $isAdded = true;
        } else {
            $isAdded = false;
        }

        return $this->generateResponse($isAdded);
    }

    /**
     * @return JsonResponse
     */
    public function getItemsCountAction()
    {
        return new JsonResponse(['count' => count($this->cart->getCartItemsArray())]);
    }
}
