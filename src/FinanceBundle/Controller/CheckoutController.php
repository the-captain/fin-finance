<?php

namespace FinanceBundle\Controller;

use FinanceBundle\Entity\Order;
use Symfony\Component\HttpFoundation\Request;

class CheckoutController extends AbstractController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function cartAction()
    {
        $data = $this->get('finance.cart.items')->getItemsFromSession();

        return $this->render(
            '@Finance/Checkout/cart.html.twig',
            [
                'items' => $data['items'],
                'sum' => $data['costSum'],
            ]
        );
    }

    /**
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function userDetailsAction(Request $request)
    {
        $data = $this->get('finance.cart.items')->getItemsFromSession();

        if (empty($data['items'])) {
            return $this->redirectToRoute('finance_user_cart');
        }

        $order = new Order();
        $form = $this->createForm('FinanceBundle\Form\Type\OrderType', $order);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $order = $this->get('finance.order')->createOrder($data['items'], $order);
            $request->getSession()->set('orderId', $order->getId());
            $this->get('logger')->addInfo('New order created', [$order]);
            if ($order->getPayment() === 1) {
                return $this->redirectToRoute('finance_mypos');
            }
        }

        return $this->render(
            '@Finance/Checkout/details.html.twig',
            [
                'items' => $data['items'],
                'sum' => $data['costSum'],
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function finalStepAction(Request $request)
    {
        if (!$request->getSession()->has('orderId')) {
            throw $this->createAccessDeniedException();
        }

        /** @var Order $order */
        $order = $this->getRep('FinanceBundle:Order')->findOneById(
            $request->getSession()->get('orderId')
        );

        if (!$order) {
            throw $this->createNotFoundException();
        }

        $order->setStatus(Order::PAYED);
        $this->save($order);

        $response = $this->render(
            '@Finance/Checkout/success.html.twig',
            [
                'order' => $order,
            ]
        );

        if ($this->getUser()) {
            $this->get('finance.mailer')->sendTemplate(
                '@Finance/Emails/order/new.mjml.html.twig',
                [
                    'order' => $order,
                ],
                $this->get('translator')->trans('Ваш заказ').'#'.$order->getId(),
            'moxnrl@gmail.com'
            );
        }

        $this->get('finance.mailer')->sendTemplate(
            '@Finance/Emails/order/manager.mjml.html.twig',
            [
                'order' => $order,
            ],
            'Новый заказ #'.$order->getId(),
            $this->getRep('FinanceBundle:Config')->findOneById(6)->getValue()
        );

        $cart = $this->get('finance.cart');

        $request->getSession()->remove('orderId');
        $response->headers->setCookie($cart->clearCart());

        return $response;
    }

    /**
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function cancelAction(Request $request)
    {
        if (!$request->getSession()->has('orderId')) {
            throw $this->createAccessDeniedException();
        }

        /** @var Order $order */
        $order = $this->getRep('FinanceBundle:Order')->findOneById(
            $request->getSession()->get('orderId')
        );

        $order->setStatus(Order::CANCELED);
        $this->save($order);

        return $this->render(
            '@Finance/Checkout/cancel.html.twig'
        );
    }
}
