<?php

namespace FinanceBundle\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\View\TwitterBootstrap3View;

use FinanceBundle\Entity\ConfigLang;

/**
 * ConfigLang controller.
 *
 */
class ConfigLangController extends AbstractAdminController
{
    public function __construct()
    {
        $this->repoName = 'FinanceBundle:ConfigLang';
        $this->filterType = 'ConfigLangFilterType';
        $this->itemType = 'ConfigLangType';
        $this->type = 'lang_config';
        $this->itemClass = ConfigLang::class;
    }

    /**
     * Displays a form to edit an existing Config entity.
     */
    public function editAction(Request $request, ConfigLang $config)
    {
        return parent::edit($request, $config);
    }

    /**
     * Deletes a Config entity.
     */
    public function deleteAction(Request $request, ConfigLang $config)
    {
        return parent::delete($request, $config);
    }

    /**
     * Delete Config by id.
     */
    public function deleteByIdAction(ConfigLang $config)
    {
        return parent::deleteById($config);
    }

    /**
     * Finds and displays an Config entity.
     */
    public function showAction(ConfigLang $config)
    {
        return parent::show($config);
    }

    /**
     * Get results from paginator and get paginator view.
     *
     */
    protected function paginator($queryBuilder, Request $request)
    {
        //sorting
        $sortCol = $queryBuilder->getRootAlias().'.'.$request->get('pcg_sort_col', 'id');
        $queryBuilder->orderBy($sortCol, $request->get('pcg_sort_order', 'desc'));
        // Paginator
        $adapter = new DoctrineORMAdapter($queryBuilder, true, false);
        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage($request->get('pcg_show', 10));

        try {
            $pagerfanta->setCurrentPage($request->get('pcg_page', 1));
        } catch (\Pagerfanta\Exception\OutOfRangeCurrentPageException $ex) {
            $pagerfanta->setCurrentPage(1);
        }

        $entities = $pagerfanta->getCurrentPageResults();

        // Paginator - route generator
        $me = $this;
        $routeGenerator = function ($page) use ($me, $request) {
            $requestParams = $request->query->all();
            $requestParams['pcg_page'] = $page;
            return $me->generateUrl('admin_lang_config', $requestParams);
        };

        // Paginator - view
        $view = new TwitterBootstrap3View();
        $pagerHtml = $view->render($pagerfanta, $routeGenerator, array(
            'proximity' => 3,
            'prev_message' => 'previous',
            'next_message' => 'next',
        ));

        return array($entities, $pagerHtml);
    }
}
