<?php

namespace FinanceBundle\Controller\Admin;

use FinanceBundle\Entity\Item;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Pagerfanta\View\TwitterBootstrap3View;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

abstract class AbstractAdminController extends Controller
{
    /**
     * @var string
     */
    protected $repoName;

    /**
     * @var string
     */
    protected $filterType;

    /**
     * @var string
     */
    protected $itemType;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var string
     */
    protected $itemClass;

    /**
     * Lists all Item entities.
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->getRepository($this->repoName)->createQueryBuilder('e');

        list($filterForm, $queryBuilder) = $this->filter($queryBuilder, $request);
        list($items, $pagerHtml) = $this->paginator($queryBuilder, $request);

        return $this->render(
            sprintf('@Finance/Admin/%s/index.html.twig', $this->type),
            [
                $this->type.'s' => $items,
                'pagerHtml' => $pagerHtml,
                'filterForm' => $filterForm->createView(),
            ]
        );
    }

    /**
     * Create filter form and process filter request.
     */
    protected function filter($queryBuilder, Request $request)
    {
        $session = $request->getSession();
        $filterForm = $this->createForm('FinanceBundle\Form\Type\\'.$this->filterType);

        // Reset filter
        if ($request->get('filter_action') == 'reset') {
            $session->remove($this->filterType);
        }

        // Filter action
        if ($request->get('filter_action') == 'filter') {
            // Bind values from the request
            $filterForm->handleRequest($request);

            if ($filterForm->isValid()) {
                // Build the query from the given form object
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
                // Save filter to session
                $filterData = $filterForm->getData();
                $session->set($this->filterType, $filterData);
            }
        } else {
            // Get filter from session
            if ($session->has($this->filterType)) {
                $filterData = $session->get($this->filterType);

                foreach ($filterData as $key => $filter) { //fix for entityFilterType that is loaded from session
                    if (is_object($filter)) {
                        $filterData[$key] = $queryBuilder->getEntityManager()->merge($filter);
                    }
                }

                $filterForm = $this->createForm('FinanceBundle\Form\Type\\'.$this->filterType, $filterData);
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
            }
        }

        return [$filterForm, $queryBuilder];
    }

    /**
     * Get results from paginator and get paginator view.
     */
    protected function paginator($queryBuilder, Request $request)
    {
        //sorting
        $sortCol = $queryBuilder->getRootAlias().'.'.$request->get('pcg_sort_col', 'id');
        $queryBuilder->orderBy($sortCol, $request->get('pcg_sort_order', 'desc'));
        // Paginator
        $adapter = new DoctrineORMAdapter($queryBuilder);
        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage($request->get('pcg_show', 10));

        try {
            $pagerfanta->setCurrentPage($request->get('pcg_page', 1));
        } catch (\Pagerfanta\Exception\OutOfRangeCurrentPageException $ex) {
            $pagerfanta->setCurrentPage(1);
        }

        $entities = $pagerfanta->getCurrentPageResults();

        // Paginator - route generator
        $me = $this;
        $routeGenerator = function ($page) use ($me, $request) {
            $requestParams = $request->query->all();
            $requestParams['pcg_page'] = $page;

            return $me->generateUrl(
                sprintf('admin_%s', $this->type),
                $requestParams
            );
        };

        // Paginator - view
        $view = new TwitterBootstrap3View();
        $pagerHtml = $view->render(
            $pagerfanta,
            $routeGenerator,
            [
                'proximity' => 3,
                'prev_message' => 'previous',
                'next_message' => 'next',
            ]
        );

        return [$entities, $pagerHtml];
    }

    /**
     * Displays a form to create a new entity.
     */
    public function newAction(Request $request)
    {
        $item = new $this->itemClass();
        $form = $this->createForm('FinanceBundle\Form\Type\\'.$this->itemType, $item);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($item);
            $em->flush();

            $editLink = $this->generateUrl(
                sprintf('admin_%s_edit', $this->type),
                ['id' => $item->getId()]
            );
            $this->get('session')->getFlashBag()->add(
                'success',
                "<a href='$editLink'>Новая позиция успешно создана!</a>"
            );

            $nextAction = $request->get('submit') === 'save'
                ? sprintf('admin_%s', $this->type)
                : sprintf('admin_%s_new', $this->type);

            return $this->redirectToRoute($nextAction);
        }

        return $this->render(
            sprintf('@Finance/Admin/%s/new.html.twig', $this->type),
            [
                $this->type => $item,
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * Finds and displays a Item entity.
     */
    protected function show($item)
    {
        $deleteForm = $this->createDeleteForm($item);

        return $this->render(
            sprintf('@Finance/Admin/%s/show.html.twig', $this->type),
            [
                $this->type => $item,
                'delete_form' => $deleteForm->createView(),
            ]
        );
    }

    /**
     * Creates a form to delete a Item entity.
     *
     * @param $item Item entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($item)
    {
        return $this->createFormBuilder()
            ->setAction(
                $this->generateUrl(
                    sprintf('admin_%s_delete', $this->type),
                    ['id' => $item->getId()]
                )
            )
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * @param Request $request
     * @param $item
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    protected function edit(Request $request, $item)
    {
        $deleteForm = $this->createDeleteForm($item);
        $editForm = $this->createForm('FinanceBundle\Form\Type\\'.$this->itemType, $item);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($item);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Изменения сохранены!');

            return $this->redirectToRoute(
                sprintf('admin_%s_edit', $this->type),
                ['id' => $item->getId()]
            );
        }

        return $this->render(
            sprintf('@Finance/Admin/%s/edit.html.twig', $this->type),
            [
                $this->type => $item,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ]
        );
    }

    /**
     * @param Request $request
     * @param $item
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    protected function delete(Request $request, $item)
    {
        $form = $this->createDeleteForm($item);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($item);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'Позиция успешно удалена');
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Возникли проблемы при удалении');
        }

        return $this->redirect($this->generateUrl(sprintf('admin_%s', $this->type)));
    }

    /**
     * Delete Item by id.
     */
    protected function deleteById($item)
    {
        $em = $this->getDoctrine()->getManager();

        try {
            $em->remove($item);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'Позиция успешно удалена');
        } catch (\Exception $ex) {
            $this->get('session')->getFlashBag()->add('error', 'Возникли проблемы при удалении');
        }

        return $this->redirect($this->generateUrl(sprintf('admin_%s', $this->type)));
    }

    /**
     * Bulk Action.
     */
    public function bulkAction(Request $request)
    {
        $ids = $request->get('ids', []);
        $action = $request->get('bulk_action', 'delete');

        if ($action === 'delete') {
            try {
                $em = $this->getDoctrine()->getManager();
                $repository = $em->getRepository($this->repoName);

                foreach ($ids as $id) {
                    $item = $repository->find($id);
                    $em->remove($item);
                }
                $em->flush();

                $this->get('session')->getFlashBag()->add('success', 'Позиции успешно удалены');
            } catch (\Exception $ex) {
                $this->get('session')->getFlashBag()->add('error', 'Возникли проблемы при удалении');
            }
        }

        return $this->redirect($this->generateUrl(sprintf('admin_%s', $this->type)));
    }
}
