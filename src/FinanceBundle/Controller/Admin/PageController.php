<?php

namespace FinanceBundle\Controller\Admin;

use FinanceBundle\Controller\AbstractController;
use FinanceBundle\Entity\Page;
use FinanceBundle\Form\Type\PageType;
use Symfony\Component\HttpFoundation\Request;

/**
 * Page controller.
 */
class PageController extends AbstractController
{
    /**
     * Lists all Page entities.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        return $this->render(
            '@Finance/Admin/page/index.html.twig',
            ['pages' => $this->getRep('FinanceBundle:Page')->findAll()]
        );
    }

    /**
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $page = new Page();
        $form = $this->createForm(PageType::class, $page);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->save($page);

            $this->addFlash(
                'success',
                sprintf('Новая страница "%s" создана!', $page->getTitle())
            );

            return $this->redirectToRoute('admin_page_index');
        }

        return $this->render(
            '@Finance/Admin/page/new.html.twig',
            [
                'page' => $page,
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @param Request $request
     * @param Page    $page
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Page $page)
    {
        $deleteForm = $this->createDeleteForm($page);
        $editForm = $this->createForm(PageType::class, $page);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->save($page);

            $this->addFlash(
                'success',
                sprintf('Страница "%s" отредактирована!', $page->getTitle())
            );

            return $this->redirectToRoute('admin_page_index');
        }

        return $this->render(
            '@Finance/Admin/page/edit.html.twig',
            [
                'page' => $page,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ]
        );
    }

    /**
     * Creates a form to delete a Page entity.
     *
     * @param Page $page
     *
     * @return \Symfony\Component\Form\Form
     */
    private function createDeleteForm(Page $page)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_page_delete', ['id' => $page->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * @param Request $request
     * @param Page    $page
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, Page $page)
    {
        $form = $this->createDeleteForm($page);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->delete($page);
        }

        return $this->redirectToRoute('admin_page_index');
    }
}
