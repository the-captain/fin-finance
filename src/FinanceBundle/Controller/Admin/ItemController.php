<?php

namespace FinanceBundle\Controller\Admin;

use FinanceBundle\Entity\Item;
use FinanceBundle\Entity\ItemTranslation;
use Symfony\Component\HttpFoundation\Request;

/**
 * Item controller.
 */
class ItemController extends AbstractAdminController
{
    public function __construct()
    {
        $this->repoName = 'FinanceBundle:Item';
        $this->filterType = 'ItemFilterType';
        $this->itemType = 'ItemType';
        $this->type = 'item';
        $this->itemClass = Item::class;
    }

    /**
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function itemImportAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $items = $em->getRepository($this->repoName)->findBy(
            [
                'type' => Item::TYPE_SCOTLAND_LP_CHILD,
            ]
        );

        if ($request->isMethod('POST')) {
            $itemsForParse = explode(PHP_EOL, $request->request->get('items'));
            foreach ($itemsForParse as $item) {
                $item = explode(';', $item);

                $initialItem = new Item();
                $initialItem->setStatus(1)
                    ->setType(Item::TYPE_SCOTLAND_LP_CHILD);

                $itemTrans = new ItemTranslation();
                $itemTrans->setName($item[0]);
                $itemTrans->setDescription($item[4]);
                $itemTrans->setLocale('en');
                $initialItem
                    ->setCost($item[1])
                    ->setCreated(new \DateTime($item[2]))
                    ->setCharterCapital($item[3])
                    ->addTranslation($itemTrans);
                $em->persist($initialItem);
            }

            $em->flush();
        }

        return $this->render(
            '@Finance/Admin/item/textarea-list.html.twig',
            [
                'items' => $items
            ]
        );
    }

    /**
     * Displays a form to edit an existing Item entity.
     */
    public function editAction(Request $request, Item $item)
    {
        return parent::edit($request, $item);
    }

    /**
     * Deletes a Item entity.
     */
    public function deleteAction(Request $request, Item $item)
    {
        return parent::delete($request, $item);
    }

    /**
     * Delete Item by id.
     */
    public function deleteByIdAction(Item $item)
    {
        return parent::deleteById($item);
    }

    /**
     * Finds and displays an Item entity.
     */
    public function showAction(Item $item)
    {
        return parent::show($item);
    }
}
