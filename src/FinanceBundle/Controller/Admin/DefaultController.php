<?php

namespace FinanceBundle\Controller\Admin;

use FinanceBundle\Controller\AbstractController;

class DefaultController extends AbstractController
{
    /**
     * Admin index page.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        return $this->render('FinanceBundle:Admin:index.html.twig');
    }
}
