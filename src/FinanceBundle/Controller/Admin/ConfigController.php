<?php

namespace FinanceBundle\Controller\Admin;

use FinanceBundle\Entity\Config;
use Symfony\Component\HttpFoundation\Request;

/**
 * Config controller.
 */
class ConfigController extends AbstractAdminController
{
    public function __construct()
    {
        $this->repoName = 'FinanceBundle:Config';
        $this->filterType = 'ConfigFilterType';
        $this->itemType = 'ConfigType';
        $this->type = 'config';
        $this->itemClass = Config::class;
    }

    /**
     * Displays a form to edit an existing Config entity.
     */
    public function editAction(Request $request, Config $config)
    {
        return parent::edit($request, $config);
    }

    /**
     * Deletes a Config entity.
     */
    public function deleteAction(Request $request, Config $config)
    {
        return parent::delete($request, $config);
    }

    /**
     * Delete Config by id.
     */
    public function deleteByIdAction(Config $config)
    {
        return parent::deleteById($config);
    }

    /**
     * Finds and displays an Config entity.
     */
    public function showAction(Config $config)
    {
        return parent::show($config);
    }
}
