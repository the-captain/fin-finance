<?php

namespace FinanceBundle\Controller\Admin;

use FinanceBundle\Entity\User;
use Symfony\Component\HttpFoundation\Request;

/**
 * User controller.
 */
class UserController extends AbstractAdminController
{
    public function __construct()
    {
        $this->repoName = 'FinanceBundle:User';
        $this->filterType = 'UserAdminFilterType';
        $this->itemType = 'UserAdminType';
        $this->type = 'user';
        $this->itemClass = User::class;
    }

    /**
     * Lists all User entities.
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->getRepository('FinanceBundle:User')->createQueryBuilder('e');

        list($filterForm, $queryBuilder) = $this->filter($queryBuilder, $request);
        list($users, $pagerHtml) = $this->paginator($queryBuilder, $request);

        return $this->render(
            '@Finance/Admin/user/index.html.twig',
            [
                'users' => $users,
                'pagerHtml' => $pagerHtml,
                'filterForm' => $filterForm->createView(),

            ]
        );
    }
    /**
     * Displays a form to edit an existing User entity.
     */
    public function editAction(Request $request, User $user)
    {
        return parent::edit($request, $user);
    }

    /**
     * Deletes a User entity.
     */
    public function deleteAction(Request $request, User $user)
    {
        return parent::delete($request, $user);
    }

    /**
     * Delete User by id.
     */
    public function deleteByIdAction(User $user)
    {
        return parent::deleteById($user);
    }

    /**
     * Finds and displays an User entity.
     */
    public function showAction(User $user)
    {
        return parent::show($user);
    }
}
