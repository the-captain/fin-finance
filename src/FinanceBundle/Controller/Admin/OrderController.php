<?php

namespace FinanceBundle\Controller\Admin;

use FinanceBundle\Entity\Order;
use Symfony\Component\HttpFoundation\Request;

/**
 * Order controller.
 */
class OrderController extends AbstractAdminController
{
    public function __construct()
    {
        $this->repoName = 'FinanceBundle:Order';
        $this->filterType = 'OrderFilterType';
        $this->itemType = 'OrderType';
        $this->type = 'order';
        $this->itemClass = Order::class;
    }

    /**
     * Displays a form to edit an existing Order entity.
     */
    public function editAction(Request $request, Order $order)
    {
        return parent::edit($request, $order);
    }

    /**
     * Deletes a Order entity.
     */
    public function deleteAction(Request $request, Order $order)
    {
        return parent::delete($request, $order);
    }

    /**
     * Delete Order by id.
     */
    public function deleteByIdAction(Order $order)
    {
        return parent::deleteById($order);
    }

    /**
     * Finds and displays an Order entity.
     */
    public function showAction(Order $order)
    {
        return parent::show($order);
    }
}
