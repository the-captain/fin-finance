<?php

namespace FinanceBundle\Controller;

class PageController extends AbstractController
{
    /**
     * @param $link
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function pageAction($link)
    {
        $page = $this->getRep('FinanceBundle:Page')->findByLink($link);
        if (!$page) {
            throw $this->createNotFoundException();
        }

        return $this->render(
            '@Finance/Default/page.html.twig',
            [
                'page' => $page,
            ]
        );
    }
}
