<?php

namespace FinanceBundle\Controller;

use FinanceBundle\Entity\Item;

/**
 * Item controller.
 */
class ItemController extends AbstractController
{
    private const PAGE_MERCHANT_ID = 5;
    private const PAGE_READY_COMPANIES_ID = 1;
    private const PAGE_BANK_ACCOUNT_ID = 4;
    private const PAGE_SPECIAL_OFFERS_ID = 7;

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function merchantAccountPageAction()
    {
        $page = $this->getRep('FinanceBundle:Page')->findOneById(self::PAGE_MERCHANT_ID);
        $items = $this->getRep('FinanceBundle:Item')->findByType(Item::TYPE_MERCHANT);
        if (!$page || !$items) {
            throw $this->createNotFoundException();
        }

        return $this->render(
            '@Finance/Item/merchantAccountPage.html.twig',
            [
                'items' => $items,
                'page' => $page,
            ]
        );
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function readyCompaniesPageAction()
    {
        $page = $this->getRep('FinanceBundle:Page')->findOneById(self::PAGE_READY_COMPANIES_ID);
        $items = $this->getRep('FinanceBundle:Item')->findBy(
            [
                'type' => Item::TYPE_COMPANY,
                'status' => Item::AVAILABLE,
            ]
        );
        if (!$page || !$items) {
            throw $this->createNotFoundException();
        }

        $subItems = $this->getRep('FinanceBundle:Item')->findBy(
            [
                'type' => Item::TYPE_SCOTLAND_LP_CHILD,
                'status' => Item::AVAILABLE,
            ]
        );

        return $this->render(
            '@Finance/Item/readyCompaniesPage.html.twig',
            [
                'items' => $items,
                'subitems' => $subItems,
                'page' => $page,
            ]
        );
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function bankAccountPageAction()
    {
        $page = $this->getRep('FinanceBundle:Page')->findOneById(self::PAGE_BANK_ACCOUNT_ID);
        $items = $this->getRep('FinanceBundle:Item')->findByType(Item::TYPE_BANK);
        if (!$page || !$items) {
            throw $this->createNotFoundException();
        }

        return $this->render(
            '@Finance/Item/bankAccountPage.html.twig',
            [
                'items' => $items,
                'page' => $page,
            ]
        );
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function specialOffersPageAction()
    {
        $page = $this->getRep('FinanceBundle:Page')->findOneById(self::PAGE_SPECIAL_OFFERS_ID);
        $items = $this->getRep('FinanceBundle:Item')->findByType(Item::TYPE_SPECIAL_OFFERS);
        if (!$page || !$items) {
            throw $this->createNotFoundException();
        }

        $companies = $this->getRep('FinanceBundle:Item')->findBy(
            [
                'type' => Item::TYPE_SCOTLAND_LP_CHILD,
                'status' => Item::AVAILABLE,
            ]
        );
        $banks = $this->getRep('FinanceBundle:Item')->findByType(Item::TYPE_BANK);

        return $this->render(
            '@Finance/Item/specialOffersPage.html.twig',
            [
                'items' => $items,
                'companies' => $companies,
                'banks' => $banks,
                'page' => $page,
            ]
        );
    }
}
