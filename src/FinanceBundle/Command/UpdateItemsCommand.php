<?php

namespace FinanceBundle\Command;

use FinanceBundle\Entity\Item;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateItemsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('update-items');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.default_entity_manager');
        $orders = $em->getRepository('FinanceBundle:Order')
            ->getUnpaidWithCompany();

        $i = 0;
        foreach ($orders as $order) {
            foreach ($order->getItems() as $item) {
                if ($item->getInitialItemId()) {
                    $originalItem = $em->getRepository('FinanceBundle:Item')
                        ->findOneById($item->getInitialItemId());
                    if ($originalItem) {
                        $originalItem->setStatus(Item::AVAILABLE);
                        $item->setSoldStatus(1);
                        $em->persist($originalItem);
                        $i++;
                    }
                }
            }
        }

        $em->flush();

        $output->writeln(sprintf('Done! Updated %s items', $i));
    }
}
