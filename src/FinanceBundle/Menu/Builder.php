<?php

namespace FinanceBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class Builder implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param FactoryInterface $factory
     * @param array            $options
     *
     * @return \Knp\Menu\ItemInterface
     */
    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $authChecker = $this->container->get('security.authorization_checker');
        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav navbar-nav');
        $menu->addChild('На сайт', ['route' => 'finance_homepage']);

        if ($authChecker->isGranted('ROLE_EDITOR') !== false) {
            $menu->addChild('Страницы', ['route' => 'admin_page_index']);
            $menu->addChild('Переводы', ['route' => 'lexik_translation_overview']);
        }
        if ($authChecker->isGranted('ROLE_ADMIN') !== false) {
            $menu->addChild('Пользователи', ['route' => 'admin_user']);

            $menu->addChild('Конфигурация', ['route' => 'admin_config', 'attributes' => ['class' => 'dropdown']]);
            $menu['Конфигурация']->setLinkAttributes(['class' => 'dropdown-toggle', 'data-toggle' => 'dropdown']);
            $menu['Конфигурация']->setChildrenAttributes(['class' => 'dropdown-menu']);
            $menu['Конфигурация']->addChild(
                'Имейлов',
                ['route' => 'admin_config']
            );
            $menu['Конфигурация']->addChild(
                'Языков',
                ['route' => 'admin_lang_config']
            );
        }

        if ($authChecker->isGranted('ROLE_MANAGER') !== false) {
            $menu->addChild('Товары', ['route' => 'admin_item', 'attributes' => ['class' => 'dropdown']]);

            $menu['Товары']->setLinkAttributes(['class' => 'dropdown-toggle', 'data-toggle' => 'dropdown']);
            $menu['Товары']->setChildrenAttributes(['class' => 'dropdown-menu']);

            $menu['Товары']->addChild(
                'Список',
                ['route' => 'admin_item']
            );
            $menu['Товары']->addChild(
                'Импорт',
                ['route' => 'admin_item_import']
            );

            $menu->addChild('Заказы', ['route' => 'admin_order']);
        }

        return $menu;
    }
}
