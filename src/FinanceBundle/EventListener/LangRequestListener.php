<?php

namespace FinanceBundle\EventListener;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class LangRequestListener implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    /**
     * @var EntityManager
     */
    private $em;

    private $lang;

    /**
     * CartCounter constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Detects that health check was requested and stops propagation of next events
     *
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        if ($event->isMasterRequest()) {
            $lang = $this->getLocale($event->getRequest()->getLocale());

            if (!$lang) {
                return;
            }

            if (!$lang->getValue()) {
                $event->setResponse(
                    new RedirectResponse(
                        str_replace(
                            sprintf('/%s/', $event->getRequest()->getLocale()),
                            '/en/',
                            $event->getRequest()->getPathInfo()
                        )
                    )
                );
            }
        }
    }

    /**
     * @param $locale
     * @return bool|string
     */
    private function getLocale($locale)
    {
        $langs = $this->em->getRepository('FinanceBundle:ConfigLang')->findAll();
        foreach ($langs as $lang) {
            if ($lang->getKey() == $locale) {
                return $lang;
            }
        }

        return false;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => ['onKernelRequest', PHP_INT_MIN],
        ];
    }
}
