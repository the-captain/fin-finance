$(function () {

    $(window).on('load', function (e) {
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
            $('body').addClass('ios');
        }

        $('body').removeClass('loaded');
    });

    $('input, textarea').each(function () {
        var placeholder = $(this).attr('placeholder');
        $(this).focus(function () {
            $(this).attr('placeholder', '');
            return false;
        });
        $(this).focusout(function () {
            $(this).attr('placeholder', placeholder);
            return false;
        });
    });

    $('.button-menu').click(function () {
        $(this).toggleClass('active'),
            $(".js-list-language").fadeOut(0);
        $('nav').slideToggle();
        return false;
    });

    $('.box-language__link').click(function () {
        $(this).parents('header').find('.js-list-language').fadeToggle(100);
        $('.box-language__link').toggleClass('active');
        $(".ios nav").fadeOut(0);
        return false;
    });

    if ($('.js-styled, .styled').length) {
        $('.js-styled, .styled').iCheck({
            checkboxClass: 'icheckbox_square-grey',
            radioClass: 'iradio_square-grey'
        });
    }

    $('.list-sections-faq__link').click(function () {
        $(this).parent().toggleClass('open');
        $(this).parent().find('.list-faq').slideToggle();
        $(this).parent().siblings().removeClass('open');
        $(this).parent().siblings().find('.list-faq').slideUp();

        return false;
    });
    $('.list-faq__link').click(function () {
        $(this).parent().toggleClass('open');
        $(this).parent().find('.list-faq__drop').slideToggle();
        $(this).parent().siblings().removeClass('open');
        $(this).parent().siblings().find('.list-faq__drop').slideUp();

        return false;
    });

    $('a.cookie-agree').click(function () {
        Cookies.set("cookieAgree", 1, {expires: 999});
        $('div.cookies-message').hide();

        return false;
    });

    $('.shopping-background').click(function () {
        $('.add-to-cart-message, .shopping-background, .add-to-cart-offshore, .add-to-cart-special').addClass('hide');
    });

    $('body').click(function (e) {
        if (!$(e.target).closest('.js-list-language').length > 0 ) {
            var langMenu = $('.box-drop.js-list-language');

            if (langMenu.is(':visible')) {
                langMenu.hide();
            }
        }
    });
});
