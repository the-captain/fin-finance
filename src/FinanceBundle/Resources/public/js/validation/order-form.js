$("#order-form").validate({
    rules: {
        "order[firstName]": {
            required: true,
            minlength: 2,
            maxlength: 200
        },
        "order[lastName]": {
            required: true,
            minlength: 2,
            maxlength: 200
        },
        "order[country]": {
            required: true,
            minlength: 2,
            maxlength: 200
        },
        "order[city]": {
            required: true,
            minlength: 2,
            maxlength: 200
        },
        "order[street]": {
            required: true,
            minlength: 2,
            maxlength: 200
        },
        "order[phone]": {
            required: true,
            minlength: 9,
            maxlength: 200,
            phone: true
        },
        "order[zip]": {
            required: true,
            minlength: 2,
            maxlength: 200
        },
        "order[payment]": {
            required: true
        },
        "_remember_me": {
            required: true
        }
    },
    messages: {
        "order[firstName]": {
            required: errorFormTranslate.required,
            minlength: jQuery.validator.format(errorFormTranslate.minlength),
            maxlength: jQuery.validator.format(errorFormTranslate.maxlength)
        },
        "order[lastName]": {
            required: errorFormTranslate.required,
            minlength: jQuery.validator.format(errorFormTranslate.minlength),
            maxlength: jQuery.validator.format(errorFormTranslate.maxlength)
        },
        "order[country]": {
            required: errorFormTranslate.required,
            minlength: jQuery.validator.format(errorFormTranslate.minlength),
            maxlength: jQuery.validator.format(errorFormTranslate.maxlength),
        },
        "order[city]": {
            required: errorFormTranslate.required,
            minlength: jQuery.validator.format(errorFormTranslate.minlength),
            maxlength: jQuery.validator.format(errorFormTranslate.maxlength),
        },
        "order[street]": {
            required: errorFormTranslate.required,
            minlength: jQuery.validator.format(errorFormTranslate.minlength),
            maxlength: jQuery.validator.format(errorFormTranslate.maxlength),
        },
        "order[phone]": {
            required: errorFormTranslate.required,
            minlength: jQuery.validator.format(errorFormTranslate.minlength),
            maxlength: jQuery.validator.format(errorFormTranslate.maxlength),
            phone: errorFormTranslate.phone
        },
        "order[zip]": {
            required: errorFormTranslate.required,
            minlength: jQuery.validator.format(errorFormTranslate.minlength),
            maxlength: jQuery.validator.format(errorFormTranslate.maxlength),
        },
        "order[payment]": {
            required: errorFormTranslate.order_payment_required,
        },
        "_remember_me": {
            required: errorFormTranslate.remember_me_required,
        }
    },
    errorPlacement: function(error, element) {
        error.prependTo(element.closest('.box-field').prepend());
    }
});

$('input[name="order[phone]"]').keyup(function () {
    this.value = '+' + this.value.replace(/[^0-9\.]/g,'');
});

$('#order-form input, #order-form select, #order-form textarea').blur(function(){
    $(this).valid();
});
