$("#user_edit_form").validate({
    rules: {
        "user_edit[firstName]": {
            required: true,
            minlength: 2,
            maxlength: 200
        },
        "user_edit[lastName]": {
            required: true,
            minlength: 2,
            maxlength: 200
        },
        "user_edit[phone]": {
            required: true,
            minlength: 9,
            maxlength: 200,
            phone: true
        }
    },
    messages: {
        "user_edit[firstName]": {
            required: errorFormTranslate.required,
            minlength: jQuery.validator.format(errorFormTranslate.minlength),
            maxlength: jQuery.validator.format(errorFormTranslate.maxlength)
        },
        "user_edit[lastName]": {
            required: errorFormTranslate.required,
            minlength: jQuery.validator.format(errorFormTranslate.minlength),
            maxlength: jQuery.validator.format(errorFormTranslate.maxlength)
        },
        "user_edit[phone]": {
            required: errorFormTranslate.required,
            minlength: jQuery.validator.format(errorFormTranslate.minlength),
            maxlength: jQuery.validator.format(errorFormTranslate.maxlength),
            phone: errorFormTranslate.phone
        }
    },
    errorPlacement: function(error, element) {
        error.prependTo(element.closest('.box-field').prepend());
    }
});

$('input[name="user_edit[phone]"]').keyup(function () {
    this.value = '+' + this.value.replace(/[^0-9\.]/g,'');
});

$('#user_edit_form input, #user_edit_form select, #user_edit_form textarea').blur(function(){
    $(this).valid();
});
