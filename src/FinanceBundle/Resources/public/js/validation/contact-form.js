$("#contact-form").validate({
    rules: {
        "contact[name]": {
            required: true,
            minlength: 2,
            maxlength: 200
        },
        "contact[email]": {
            required: true,
            minlength: 6,
            maxlength: 200,
            email: true
        },
        "contact[phone]": {
            required: true,
            minlength: 9,
            maxlength: 200,
            phone: true
        },
        "contact[text]": {
            required: true,
            minlength: 8,
            maxlength: 2000
        }
    },
    messages: {
        "contact[name]": {
            required: errorFormTranslate.required,
            minlength: jQuery.validator.format(errorFormTranslate.minlength),
            maxlength: jQuery.validator.format(errorFormTranslate.maxlength)
        },
        "contact[email]": {
            required: errorFormTranslate.required,
            minlength: jQuery.validator.format(errorFormTranslate.minlength),
            maxlength: jQuery.validator.format(errorFormTranslate.maxlength),
            email: errorFormTranslate.email
        },
        "contact[phone]": {
            required: errorFormTranslate.required,
            minlength: jQuery.validator.format(errorFormTranslate.minlength),
            maxlength: jQuery.validator.format(errorFormTranslate.maxlength),
            phone: errorFormTranslate.phone
        },
        "contact[text]": {
            required: errorFormTranslate.required,
            minlength: jQuery.validator.format(errorFormTranslate.minlength),
            maxlength: jQuery.validator.format(errorFormTranslate.maxlength)
        }
    },
    errorPlacement: function(error, element) {
        error.prependTo(element.closest('.box-field').prepend());
    }
});

$('input[name="contact[phone]"]').keyup(function () {
    this.value = '+' + this.value.replace(/[^0-9\.]/g,'');
});

$('#contact-form input, #contact-form select, #contact-form textarea').blur(function(){
    $(this).valid();
});
