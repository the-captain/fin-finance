$("#recovery-form").validate({
    rules: {
        "user_recovery[email]": {
            required: true,
            minlength: 6,
            maxlength: 200,
            email: true
        }
    },
    messages: {
        "user_recovery[email]": {
            required: errorFormTranslate.required,
            minlength: jQuery.validator.format(errorFormTranslate.minlength),
            maxlength: jQuery.validator.format(errorFormTranslate.maxlength),
            email: errorFormTranslate.email
        }
    },
    errorPlacement: function(error, element) {
        error.prependTo(element.closest('.box-field').prepend());
    }
});
$('#auth-form input, #auth-form select, #auth-form textarea').blur(function(){
    $(this).valid();
});
