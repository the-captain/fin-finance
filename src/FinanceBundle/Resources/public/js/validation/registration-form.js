$("#registration-form").validate({
    rules: {
        "user[firstName]": {
            required: true,
            minlength: 2,
            maxlength: 200
        },
        "user[lastName]": {
            required: true,
            minlength: 2,
            maxlength: 200
        },
        "user[phone]": {
            required: true,
            minlength: 9,
            maxlength: 200,
            phone: true
        },
        "user[email]": {
            required: true,
            minlength: 6,
            maxlength: 200,
            email: true
        },
        "user[password][first]": {
            required: true,
            minlength: 6,
            maxlength: 200
        },
        "user[email][second]": {
            required: true,
            minlength: 6,
            maxlength: 200,
            equalTo: "#user_password_second"
        },
        "i_agree": {
            required: true
        }
    },
    messages: {
        "user[firstName]": {
            required: errorFormTranslate.required,
            minlength: jQuery.validator.format(errorFormTranslate.minlength),
            maxlength: jQuery.validator.format(errorFormTranslate.maxlength)
        },
        "user[lastName]": {
            required: errorFormTranslate.required,
            minlength: jQuery.validator.format(errorFormTranslate.minlength),
            maxlength: jQuery.validator.format(errorFormTranslate.maxlength)
        },
        "user[phone]": {
            required: errorFormTranslate.required,
            minlength: jQuery.validator.format(errorFormTranslate.minlength),
            maxlength: jQuery.validator.format(errorFormTranslate.maxlength),
            phone: errorFormTranslate.phone
        },
        "user[email]": {
            required: errorFormTranslate.required,
            minlength: jQuery.validator.format(errorFormTranslate.minlength),
            maxlength: jQuery.validator.format(errorFormTranslate.maxlength),
            email: errorFormTranslate.email
        },
        "user[password][first]": {
            required: errorFormTranslate.required,
            minlength: jQuery.validator.format(errorFormTranslate.minlength),
            maxlength: jQuery.validator.format(errorFormTranslate.maxlength)
        },
        "user[password][second]": {
            required: errorFormTranslate.required,
            minlength: jQuery.validator.format(errorFormTranslate.minlength),
            maxlength: jQuery.validator.format(errorFormTranslate.maxlength),
            equalTo: errorFormTranslate.password_equalTo
        },
        "i_agree": {
            required: errorFormTranslate.i_agree_required,
        }
    },
    errorPlacement: function(error, element) {
        error.prependTo(element.closest('.box-field').prepend());
    }
});

$('input[name="user[phone]"]').keyup(function () {
    this.value = '+' + this.value.replace(/[^0-9\.]/g,'');
});

$('#registration-form input, #registration-form select, #registration-form textarea').blur(function(){
    $(this).valid();
});
