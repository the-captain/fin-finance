$("#auth-form").validate({
    rules: {
        "_username": {
            required: true,
            minlength: 6,
            maxlength: 200,
            email: true
        },
        "_password": {
            required: true,
            minlength: 6,
            maxlength: 200
        }
    },
    messages: {
        "_username": {
            required: errorFormTranslate.required,
            minlength: jQuery.validator.format(errorFormTranslate.minlength),
            maxlength: jQuery.validator.format(errorFormTranslate.maxlength),
            email: errorFormTranslate.email
        },
        "_password": {
            required: errorFormTranslate.required,
            minlength: jQuery.validator.format(errorFormTranslate.minlength),
            maxlength: jQuery.validator.format(errorFormTranslate.maxlength)
        }
    },
    errorPlacement: function(error, element) {
        error.prependTo(element.closest('.box-field').prepend());
    }
});
$('#auth-form input, #auth-form select, #auth-form textarea').blur(function(){
    $(this).valid();
});
