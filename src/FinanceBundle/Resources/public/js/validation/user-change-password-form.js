$("#user_change_password").validate({
    rules: {
        "user_change_password[password]": {
            required: true,
            minlength: 6,
            maxlength: 200
        },
        "user_change_password[new_password][first]": {
            required: true,
            minlength: 6,
            maxlength: 200
        },
        "user_change_password[new_password][second]": {
            required: true,
            minlength: 6,
            maxlength: 200,
            equalTo: "#user_change_password_new_password_first"
        }
    },
    messages: {
        "user_change_password[password]": {
            required: errorFormTranslate.required,
            minlength: jQuery.validator.format(errorFormTranslate.minlength),
            maxlength: jQuery.validator.format(errorFormTranslate.maxlength)
        },
        "user_change_password[new_password][first]": {
            required: errorFormTranslate.required,
            minlength: jQuery.validator.format(errorFormTranslate.minlength),
            maxlength: jQuery.validator.format(errorFormTranslate.maxlength)
        },
        "user_change_password[new_password][second]": {
            required: errorFormTranslate.required,
            minlength: jQuery.validator.format(errorFormTranslate.minlength),
            maxlength: jQuery.validator.format(errorFormTranslate.maxlength),
            equalTo: errorFormTranslate.password_equalTo
        }
    },
    errorPlacement: function(error, element) {
        error.prependTo(element.closest('.box-field').prepend());
    }
});
$('#user_change_password input, #user_change_password select, #user_change_password textarea').blur(function(){
    $(this).valid();
});
