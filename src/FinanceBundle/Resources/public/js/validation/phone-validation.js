$.validator.addMethod( "phone", function( phone_number, element ) {
    phone_number = phone_number.replace(/\s/g,'');
    return this.optional( element ) || phone_number.match( /^(\+)[0-9]*$/ );
}, "Please specify a valid phone number" );