$(function () {

    $("[data-select]").click(function () {
        $('.add-to-cart-offshore, .shopping-background').removeClass('hide');
    });

    $('input.styled').on('ifChanged', function () {
        $(".popup__price").html(shoppingOffshoreUpdate().sum);
    });

    $('.add-to-cart-offshore .close-popup').click(function (e) {
        $('.add-to-cart-offshore, .shopping-background').addClass('hide');
        return false;
    });

    $("[data-offshore-buy]").click(function () {
        var ids = shoppingOffshoreUpdate().ids;
        var basketCounter = $('.basket-counter');
        $.ajax({
            type: 'POST',
            url: '/cart/add/array',
            data: {ids: ids},
            success: function () {
                $('.add-to-cart-offshore').addClass('hide');
                $('.add-to-cart-message').removeClass('hide');
                basketCounter.html(parseInt(basketCounter.text()) + 1);
            }
        });
    });

    function shoppingOffshoreUpdate() {
        var ids = [];
        var sum = 0;

        $(".checkbox_popup:checked").each(function (el, item) {
            ids.push($(item).attr("data-id"));
            sum += parseInt($(item).attr("data-price"), 10);
        });

        return {"ids": ids, "sum": sum};
    }
});
