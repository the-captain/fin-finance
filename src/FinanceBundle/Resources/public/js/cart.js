$(function () {

    $(".remove-from-cart").click(function () {
        $(this).parent().parent().hide();

        var button = $(this);
        var id = button.attr("data-item-id");
        $('li#' + id).hide();

        $.ajax({
            type: 'DELETE',
            url: '/cart/delete/' + id,
            success: function () {

                var cost = parseInt(button.attr("data-item-cost"));
                var costLine = $('.line-total-form__price > span');
                var oldCost = parseInt(costLine.text());

                costLine.html(parseInt(oldCost - cost));

                var basketCounter = $('.basket-counter');
                basketCounter.html(parseInt(basketCounter.text()) - 1);

            }
        });

        return false;
    });

});
