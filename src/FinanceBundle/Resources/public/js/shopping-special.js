$(function () {

    var id = 0;

    $("[data-select-special-offer]").click(function () {
        $('.add-to-cart-special, .shopping-background').removeClass('hide');
        $(".popup__price").html($(this).attr("data-special-offer-price"));
        id = $(this).attr("data-select-special-offer");
    });

    $('.add-to-cart-special .close-popup').click(function (e) {
        $('.add-to-cart-special, .shopping-background').addClass('hide');
        return false;
    });

    $("[data-special-buy]").click(function (e) {

        var ids = shoppingOffshoreUpdate().ids;
        var basketCounter = $('.basket-counter');
        if (ids.length > 1) {
            $.ajax({
                type: 'POST',
                url: '/cart/add/special/' + id,
                data: {ids: ids},
                success: function () {
                    $('.add-to-cart-special').addClass('hide');
                    $('.add-to-cart-message').removeClass('hide');
                    basketCounter.html(parseInt(basketCounter.text()) + 1);
                }
            });
            e.preventDefault();
        }
    });

    function shoppingOffshoreUpdate() {
        var ids = [];
        var sum = 0;
        $(".js-styled:checked").each(function () {

            ids.push($(this).attr("data-id"));
            sum += parseInt($(this).attr("data-price"), 10);
        });

        return {"ids": ids, "sum": sum};
    }
});
