$(function () {
    $('input[type=radio].js-styled').on('ifChanged', function () {
        if (parseInt($(this).val()) === 0) {
            $('.paypal-button').removeClass('hidden');
            $('.button.next').addClass('hidden');
        } else {
            $('.paypal-button').addClass('hidden');
            $('.button.next').removeClass('hidden');
        }

    });
});
