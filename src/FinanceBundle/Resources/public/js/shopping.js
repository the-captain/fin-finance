$(function () {

    $("[data-buy]").click(function () {
        var id = $(this).attr("data-buy");
        var basketCounter = $('.basket-counter');
        $.ajax({
            type: 'POST',
            url: '/cart/add/' + id,
            success: function () {
                $('.add-to-cart-message, .shopping-background').removeClass('hide');
                basketCounter.html(parseInt(basketCounter.text()) + 1);
            }
        });
    });

    $('.add-to-cart-message .close-popup').click(function (e) {
        $('.add-to-cart-message, .shopping-background').addClass('hide');
        return false;
    });
});
