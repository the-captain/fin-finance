$(function () {

    $("[data-bank-buy]").click(function () {
        var id = $(this).attr("data-bank-buy");
        var basketCounter = $('.basket-counter');
        var type = $('input[name=' + id + ']:checked', 'ul.list-bank').val();

        if (type) {
            $.ajax({
                type: 'POST',
                url: '/cart/add/bank/' + id + '/' + type,
                success: function () {
                    $('.add-to-cart-message, .shopping-background').removeClass('hide');
                    basketCounter.html(parseInt(basketCounter.text()) + 1);
                }
            });
        }
    });

    $('body').click(function (e) {
        if (!$(e.target).closest('.list-radio.for-bank').length > 0 ) {
            $('input[type="radio"]').prop('checked', false);
            $('.iradio_square-grey').removeClass('checked');
        }
    });
});
