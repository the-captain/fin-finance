$(function () {
    var meter = $('#meter');
    var phone = $("#user_phone, #user_edit_phone, #order_phone, #contact_phone");

    if (meter.length) {
        meter.entropizer({
            target: '#' + meter.attr("data-target"),

            update: function (data, ui) {
                ui.bar.css({
                    'background-color': data.color,
                    'width': data.percent + '%'
                });
            }
        });
    }

    if (phone.length) {
        phone.intlTelInput({
            allowDropdown: false,
            autoPlaceholder: 'aggressive',
            nationalMode: false,
            separateDialCode: false,
            initialCountry: "auto",
            geoIpLookup: function(callback) {
                $.get('https://ipinfo.io', function() {}, "jsonp").always(function(resp) {
                    var countryCode = (resp && resp.country) ? resp.country : "";
                    callback(countryCode);
                });
            }
        });
    }
});
