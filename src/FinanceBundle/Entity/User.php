<?php

namespace FinanceBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(
 *     name="users",
 *     indexes={@ORM\Index(name="login_idx", columns={"email"})}
 * )
 * @UniqueEntity(fields={"email"}, message="У вас уже есть аккаунт")
 * @ORM\Entity(repositoryClass="FinanceBundle\Repository\UserRepository")
 * @ORM\Entity()
 * @ORM\Cache("NONSTRICT_READ_WRITE")
 */
class User implements UserInterface, \Serializable, EquatableInterface
{
    /**
     * @var string
     *
     * @Assert\NotNull()
     * @ORM\Column(type="string", length=255)
     */
    public $firstName;

    /**
     * @var string
     *
     * @Assert\NotNull()
     * @ORM\Column(type="string", length=255)
     */
    public $lastName;

    /**
     * @var array
     *
     * @ORM\OneToMany(targetEntity="Order", mappedBy="user", cascade={"persist", "remove"})
     */
    protected $orders;

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotNull()
     * @Assert\Length(min = 6)
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $resetHash;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $confirmationHash;

    /**
     * @var string
     *
     * @Assert\NotNull()
     * @Assert\Email()
     *
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $email;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=50)
     */
    private $phone;

    /**
     * @var array
     *
     * @ORM\Column(type="array")
     */
    private $roles;

    /**
     * @var array
     *
     * @ORM\Column(type="array")
     */
    private $cart;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isConfirmed;

    /**
     * @var string
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created;

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->roles = [];
        $this->orders = new ArrayCollection();
        $this->setConfirmed(false);
        $this->created = new \DateTime();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     *
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get user data for authorization.
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->email;
    }

    /**
     * Get user data for authorization.
     */
    public function getSalt(): void
    {
        return;
    }

    /**
     * Get user data for authorization.
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     *
     * @return $this
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    public function eraseCredentials()
    {
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(
            [
                $this->id,
                $this->email,
                $this->password,
            ]
        );
    }

    /**
     * @see \Serializable::unserialize()
     *
     * @param string $serialized
     */
    public function unserialize($serialized)
    {
        list(
            $this->id,
            $this->email,
            $this->password
            ) = unserialize($serialized);
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     *
     * @return $this
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     *
     * @return $this
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return int
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     *
     * @return $this
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return string
     */
    public function getResetHash()
    {
        return $this->resetHash;
    }

    /**
     * @param string $resetHash
     *
     * @return $this
     */
    public function setResetHash($resetHash)
    {
        $this->resetHash = $resetHash;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getOrders()
    {
        return $this->orders->getValues();
    }

    /**
     * @param Order $order
     */
    public function setOrder(Order $order)
    {
        $this->addOrder($order);
    }

    /**
     * @param Order $order
     */
    public function addOrder(Order $order)
    {
        $this->orders->add($order);
    }

    /**
     * @param Order $order
     *
     * @return $this
     */
    public function removeOrder(Order $order)
    {
        $this->orders->removeElement($order);

        return $this;
    }

    /**
     * @param UserInterface $user
     *
     * @return bool
     */
    public function isEqualTo(UserInterface $user)
    {
        if ($user instanceof self) {
            // Check that the roles are the same, in any order
            $isEqual = count($this->getRoles()) == count($user->getRoles());
            if ($isEqual) {
                foreach ($this->getRoles() as $role) {
                    $isEqual = $isEqual && in_array($role, $user->getRoles());
                }
            }

            return $isEqual;
        }

        return false;
    }

    /**
     * Get User ROLES.
     *
     * @return array
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Set roles.
     *
     * @param array $roles
     *
     * @return User
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @return array
     */
    public function getCart()
    {
        return $this->cart;
    }

    /**
     * @param array $cart
     *
     * @return $this
     */
    public function setCart(array $cart)
    {
        $this->cart = $cart;

        return $this;
    }

    /**
     * @return bool
     */
    public function isConfirmed()
    {
        return $this->isConfirmed;
    }

    /**
     * @param $isConfirmed
     * @return $this
     */
    public function setConfirmed($isConfirmed)
    {
        $this->isConfirmed = $isConfirmed;

        return $this;
    }

    /**
     * @return string
     */
    public function getConfirmationHash(): string
    {
        return $this->confirmationHash;
    }

    /**
     * @param string $confirmationHash
     * @return $this
     */
    public function setConfirmationHash(string $confirmationHash)
    {
        $this->confirmationHash = $confirmationHash;

        return $this;
    }

    /**
     * @return string
     */
    public function getCreated(): string
    {
        return $this->created;
    }

    /**
     * @param string $created
     * @return $this
     */
    public function setCreated(string $created)
    {
        $this->created = $created;

        return $this;
    }
}
