<?php

namespace FinanceBundle\Entity;

trait ItemOperations
{
    /**
     * @var int
     *
     * @ORM\Column(name="cost", type="integer")
     */
    private $cost;

    /**
     * @var int
     *
     * @ORM\Column(name="type", type="integer")
     */
    private $type;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;

    /**
     * @var int
     *
     * @ORM\Column(name="distantCost", type="integer", nullable=true)
     */
    private $distantCost;

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return Item
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     *
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return int
     */
    public function getOrderCost()
    {
        return $this->getDistantCost() ?: $this->getCost();
    }

    /**
     * Get distantCost.
     *
     * @return int
     */
    public function getDistantCost()
    {
        return $this->distantCost;
    }

    /**
     * Set distantCost.
     *
     * @param int $distantCost
     *
     * @return Item
     */
    public function setDistantCost($distantCost)
    {
        $this->distantCost = $distantCost;

        return $this;
    }

    /**
     * Get cost.
     *
     * @return int
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Set cost.
     *
     * @param int $cost
     *
     * @return Item
     */
    public function setCost($cost)
    {
        $this->cost = $cost;

        return $this;
    }
}
