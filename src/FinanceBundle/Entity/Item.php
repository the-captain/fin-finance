<?php

namespace FinanceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Table(name="item")
 * @ORM\Entity(repositoryClass="FinanceBundle\Repository\ItemRepository")
 * @ORM\Cache("NONSTRICT_READ_WRITE")
 */
class Item
{
    use ItemOperations;
    use ORMBehaviors\Translatable\Translatable;

    // Types
    public const TYPE_COMPANY = 0;
    public const TYPE_BANK = 1;
    public const TYPE_MERCHANT = 2;
    public const TYPE_SCOTLAND_LP_CHILD = 3;
    public const TYPE_SPECIAL_OFFERS = 4;

    // Buy status
    public const AVAILABLE = 1;
    public const ITEM_SOLD = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer")
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=255, nullable=true)
     */
    private $country;

    /**
     * @var int
     *
     * @ORM\Column(name="charterCapital", type="integer", nullable=true)
     */
    private $charterCapital;

    /**
     * @var array
     */
    private $childItems = [];

    /**
     * @var bool
     */
    private $isDist = 1;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->translate()->getName();
    }

    /**
     * Get country.
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set country.
     *
     * @param string $country
     *
     * @return Item
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->translate()->getDescription();
    }

    /**
     * @return int
     */
    public function getCharterCapital()
    {
        return $this->charterCapital;
    }

    /**
     * @param int $charterCapital
     *
     * @return $this
     */
    public function setCharterCapital($charterCapital)
    {
        $this->charterCapital = $charterCapital;

        return $this;
    }

    /**
     * @return array
     */
    public function getChildItems(): array
    {
        return $this->childItems;
    }

    /**
     * @param Item $childItem
     *
     * @return $this
     */
    public function setChildItem($childItem)
    {
        $this->childItems[] = $childItem;

        return $this;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     *
     * @return $this
     */
    public function setStatus(int $status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return bool
     */
    public function isDist(): bool
    {
        return $this->isDist;
    }

    /**
     * @param bool $isDist
     * @return $this
     */
    public function setIsDist(bool $isDist)
    {
        $this->isDist = $isDist;

        return $this;
    }

    /**
     * @return bool
     */
    public function isBankItem()
    {
        return $this->getType() === self::TYPE_BANK;
    }
}
