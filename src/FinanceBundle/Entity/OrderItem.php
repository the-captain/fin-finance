<?php

namespace FinanceBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="order_items")
 * @ORM\Entity(repositoryClass="FinanceBundle\Repository\OrderItemRepository")
 * @ORM\Cache("NONSTRICT_READ_WRITE")
 */
class OrderItem
{
    use ItemOperations;

    /**
     * @ORM\OneToMany(targetEntity="OrderItem", mappedBy="parent", cascade={"persist"})
     */
    protected $childItems;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=255, nullable=true)
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="initialItemId", type="integer", nullable=true)
     */
    private $initialItemId;

    /**
     * @ORM\ManyToOne(targetEntity="Order", inversedBy="items")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     * @ORM\Cache("NONSTRICT_READ_WRITE")
     */
    private $order;

    /**
     * @ORM\ManyToOne(targetEntity="OrderItem", inversedBy="childItems", cascade={"persist"})
     * @ORM\JoinColumn(name="parent", referencedColumnName="id")
     */
    private $parent;

    /**
     * @var integer
     *
     * @ORM\Column(name="isBankItem", type="integer", nullable=true)
     */
    private $isBankItem;

    /**
     * @var bool
     *
     * @ORM\Column(name="isDist", type="integer", nullable=true)
     */
    private $isDist = 1;

    /**
     * @var bool
     *
     * @ORM\Column(name="soldStatus", type="integer", nullable=true)
     */
    private $soldStatus = 0;

    public function __construct()
    {
        $this->children = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get country.
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set country.
     *
     * @param string $country
     *
     * @return $this
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param Order $order
     *
     * @return $this
     */
    public function setOrder(Order $order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * @return array
     */
    public function getChildItems()
    {
        return $this->childItems;
    }

    /**
     * @param OrderItem $childItem
     *
     * @return $this
     */
    public function setChildItems(OrderItem $childItem)
    {
        $this->childItems[] = $childItem;
        $childItem->setParent($this);

        return $this;
    }

    /**
     * @return OrderItem
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param OrderItem $parent
     *
     * @return $this
     */
    public function setParent(OrderItem $parent)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return int
     */
    public function getInitialItemId()
    {
        return $this->initialItemId;
    }

    /**
     * @param int $initialItemId
     * @return $this
     */
    public function setInitialItemId(int $initialItemId)
    {
        $this->initialItemId = $initialItemId;

        return $this;
    }

    /**
     * @return int
     */
    public function isBankItem()
    {
        return $this->isBankItem;
    }

    /**
     * @param int $isBankItem
     *
     * @return $this
     */
    public function setBankItem(int $isBankItem)
    {
        $this->isBankItem = $isBankItem;

        return $this;
    }

    /**
     * @return bool
     */
    public function isDist(): bool
    {
        return $this->isDist;
    }

    /**
     * @param bool $isDist
     * @return $this
     */
    public function setDist(bool $isDist)
    {
        $this->isDist = $isDist;

        return $this;
    }

    /**
     * @return bool
     */
    public function isSoldStatus(): bool
    {
        return $this->soldStatus;
    }

    /**
     * @param bool $soldStatus
     *
     * @return $this
     */
    public function setSoldStatus(bool $soldStatus)
    {
        $this->soldStatus = $soldStatus;

        return $this;
    }
}
