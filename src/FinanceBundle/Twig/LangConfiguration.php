<?php

namespace FinanceBundle\Twig;

use Doctrine\ORM\EntityManager;

class LangConfiguration extends \Twig_Extension
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var array
     */
    private $lang;

    /**
     * CartCounter constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('is_lang_available', [$this, 'checkLang']),
        ];
    }

    /**
     * @param string $lang
     * @return bool
     */
    public function checkLang(string $lang): bool
    {
        if (!$this->lang) {
            $this->lang = $this->em->getRepository('FinanceBundle:ConfigLang')->findAll();
        }

        foreach ($this->lang as $language) {
            if ($language->getKey() == $lang) {
                return $language->getValue();
            }
        }

        return false;
    }
}
