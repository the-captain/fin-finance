<?php

namespace FinanceBundle\Twig;

use FinanceBundle\Service\CartService;

class CartCounter extends \Twig_Extension
{
    /**
     * @var CartService
     */
    protected $cart;

    /**
     * @param \FinanceBundle\Service\CartService $cart
     */
    public function __construct(CartService $cart)
    {
        $this->cart = $cart;
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('cart_item_count', [$this, 'getCartItemCount']),
        ];
    }

    /**
     * @return int
     */
    public function getCartItemCount(): int
    {
        return count($this->cart->getCartItemsArray());
    }
}
