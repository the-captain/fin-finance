<?php

namespace FinanceBundle\Service;

use Doctrine\ORM\EntityManager;
use FinanceBundle\Entity\Item;
use FinanceBundle\Entity\Order;
use FinanceBundle\Entity\OrderItem;
use FinanceBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class OrderService
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @param TokenStorageInterface $user
     * @param EntityManager         $entityManager
     */
    public function __construct(
        TokenStorageInterface $tokenStorage,
        EntityManager $entityManager
    ) {
        $this->tokenStorage = $tokenStorage;
        $this->entityManager = $entityManager;
    }

    /**
     * @param $type
     * @return bool
     */
    private function isCompany($type)
    {
        return $type == Item::TYPE_COMPANY || $type == Item::TYPE_SCOTLAND_LP_CHILD;
    }

    /**
     * @param Item $item
     */
    private function setSold(Item $item): void
    {
        $originalItem = $this->entityManager->getRepository('FinanceBundle:Item')
            ->findOneById($item->getId());
        $originalItem->setStatus(Item::ITEM_SOLD);
        $this->entityManager->persist($originalItem);
    }

    /**
     * @param Item[] $items
     * @param Order  $order
     *
     * @return Order
     */
    public function createOrder($items, Order $order): Order
    {
        $order->setStatus(Order::NEW_ORDER);
        foreach ($items as $item) {
            $orderItem = $this->createItem($item, $order);

            if ($this->isCompany($item->getType())) {
                $this->setSold($item);
            }

            if ($item->getChildItems()) {
                foreach ($item->getChildItems() as $childItem) {
                    $childOrderItem = $this->createItem($childItem, $order);
                    $childOrderItem->setParent($orderItem);
                    $orderItem->setChildItems($childOrderItem);

                    if ($this->isCompany($childItem->getType())) {
                        $this->setSold($childItem);
                    }
                }
            }

            $order->addItem($orderItem);
        }

        if (is_object($this->getUser())) {
            $order->setUser($this->getUser());
        }

        $this->entityManager->persist($order);
        $this->entityManager->flush();

        return $order;
    }

    /**
     * @param Item  $item
     * @param Order $order
     *
     * @return OrderItem
     */
    private function createItem(Item $item, Order $order)
    {
        $orderItem = new OrderItem();
        $orderItem->setCost($item->getCost())
            ->setDistantCost($item->getDistantCost())
            ->setCountry($item->getCountry())
            ->setCreated($item->getCreated())
            ->setName($item->translate()->getName())
            ->setDescription($item->translate()->getDescription())
            ->setType($item->getType())
            ->setOrder($order)
            ->setBankItem($item->isBankItem())
            ->setDist($item->isDist())
            ->setInitialItemId($item->getId());

        return $orderItem;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->tokenStorage->getToken()->getUser();
    }
}
