<?php

namespace FinanceBundle\Service;

use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;

class CleanTalkService
{
    private const AUTH_KEY = 'y3aneredu2yj';
    private const AGENT = 'php-api';
    private const URL = 'http://moderate.cleantalk.org/api2.0/';
    private const USER_ERROR_KEY = 'general.cleantalk.user';
    private const CONTACT_ERROR_KEY = 'general.cleantalk.contact';

    /**
     * @return \CleantalkRequest
     */
    private function getCleantalkRequest()
    {
        $ctRequest = new \CleantalkRequest();
        $ctRequest->auth_key = self::AUTH_KEY;
        $ctRequest->agent = self::AGENT;

        return $ctRequest;
    }

    /**
     * @return \Cleantalk
     */
    private function getCleantalk()
    {
        $ct = new \Cleantalk();
        $ct->server_url = self::URL;

        return $ct;
    }

    /**
     * @param Form $form
     * @param Request $request
     *
     * @return bool
     */
    public function checkContactRequest(Form $form, Request $request)
    {
        $ctRequest = $this->getCleantalkRequest();
        $ctRequest->sender_email = $form['email']->getData();
        $ctRequest->sender_ip = $request->getClientIp();

        $ctRequest->sender_nickname = $form['name']->getData();
        $ctRequest->message = $form['text']->getData();

        $ctRequest->js_on = $form['js_on']->getData();
        $ctRequest->submit_time = time() - (int)$request->getSession()->get('ct_submit_time');

        if ($this->getCleantalk()->isAllowMessage($ctRequest)->allow) {
            return true;
        }

        $form->addError(new FormError(self::CONTACT_ERROR_KEY));

        return false;
    }

    /**
     * @param Form $form
     * @param Request $request
     *
     * @return bool
     */
    public function checkUser(Form $form, Request $request)
    {
        $ctRequest = $this->getCleantalkRequest();
        $ctRequest->sender_email = $form['email']->getData();
        $ctRequest->sender_ip = $request->getClientIp();
        $ctRequest->sender_nickname = $form['firstName']->getData().' '.$form['lastName']->getData();
        $ctRequest->js_on = $form['js_on']->getData();


        $ctRequest->submit_time = time() - (int)$request->getSession()->get('ct_submit_time');

        if ($this->getCleantalk()->isAllowUser($ctRequest)->allow) {
            return true;
        }

        $form->addError(new FormError(self::USER_ERROR_KEY));

        return false;
    }
}
