<?php

namespace FinanceBundle\Service;

use Mailgun\Mailgun;
use Symfony\Bundle\TwigBundle\TwigEngine;

class MailerService
{
    private const FROM = 'Fintech Finance <noreply@fin-fin.com>';

    /**
     * @var Mailgun
     */
    private $mailgun;

    /**
     * @var string
     */
    private $domain;

    /**
     * @var TwigEngine
     */
    private $templating;

    /**
     * @var string
     */
    private $dir = __DIR__.'/../../../var/mails/';

    /**
     * MailerService constructor.
     *
     * @param $key
     * @param $domain
     */
    public function __construct($key, $domain, TwigEngine $templating)
    {
        $this->mailgun = new Mailgun($key);
        $this->templating = $templating;
        $this->domain = $domain;
    }

    /**
     * @param $template
     * @param $data
     * @param $subject
     * @param $to
     *
     * @return \stdClass
     */
    public function sendTemplate($template, $data, $subject, $to)
    {
        return $this->send(
            $this->compileMjml($this->templating->render($template, $data)),
            $subject,
            $to
        );
    }

    /**
     * @param $text
     * @param $subject
     * @param $to
     *
     * @return \stdClass
     */
    public function send($text, $subject, $to)
    {
        return $this->mailgun->sendMessage(
            $this->domain,
            [
                'from' => self::FROM,
                'to' => $to,
                'subject' => $subject,
                'html' => $text,
                'text' => strip_tags($text),
            ]
        );
    }

    /**
     * @param $text
     *
     * @return string
     */
    private function compileMjml($text)
    {
        $fileName = uniqid('mjtemplate', false).'.mjml';
        file_put_contents($this->dir.$fileName, $text);

        return $this->runCompiler($fileName);
    }

    /**
     * @param $fileName
     *
     * @return string
     */
    private function runCompiler($fileName)
    {
        // Compile mjml template
        $html = shell_exec(
            sprintf(
                'mjml -r %s -s',
                $this->dir.$fileName
            )
        );

        unlink($this->dir.$fileName);

        return $html;
    }
}
