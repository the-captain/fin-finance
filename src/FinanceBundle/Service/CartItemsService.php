<?php

namespace FinanceBundle\Service;

use Doctrine\ORM\EntityManager;
use FinanceBundle\Entity\Item;

class CartItemsService
{
    private const DIST = 0;

    /**
     * @var CartService
     */
    private $cartService;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var array
     */
    private $items = [];

    /**
     * @var int
     */
    private $costSum = 0;

    /**
     * @param CartService   $cartService
     * @param EntityManager $entityManager
     */
    public function __construct(
        CartService $cartService,
        EntityManager $entityManager
    ) {
        $this->cartService = $cartService;
        $this->entityManager = $entityManager;
    }

    /**
     * @return []Item
     */
    public function getItemsFromSession()
    {
        if (!$this->items) {
            // Process regular items
            foreach ($this->cartService->getCartItemsArray() as $key => $itemOption) {
                $item = $this->getItem($itemOption['id']);
                $this->checkDistCost($item, $itemOption);
                $this->items[$key] = $item;
                $this->costSum += $item->getCost();
                $this->checkAndSetChilds($item, $itemOption);
            }
        }

        return [
            'items' => $this->items,
            'costSum' => $this->costSum,
        ];
    }

    /**
     * @param $id
     *
     * @return Item
     *
     * @throws \Exception
     */
    private function getItem($id)
    {
        $item = $this->entityManager
            ->getRepository('FinanceBundle:Item')
            ->findOneById($id);

        if (!$item) {
            throw new \Exception(sprintf('Item %s doesn\'t extsts', $id));
        }

        return clone $item;
    }

    /**
     * @param Item $item
     * @param $itemOption
     */
    private function checkDistCost(Item $item, $itemOption): void
    {
        if (array_key_exists('distType', $itemOption)) {
            if ($itemOption['distType'] == self::DIST) {
                $item->setCost($item->getDistantCost());
                $item->setIsDist(self::DIST);
            }
        }
    }

    /**
     * @param Item $item
     * @param $itemOption
     */
    private function checkAndSetChilds(Item $item, $itemOption): void
    {
        if (!empty($itemOption['options']['childs'])) {
            foreach ($itemOption['options']['childs'] as $child) {
                $item->setChildItem($this->getItem($child['id']));
            }
        }
    }
}
