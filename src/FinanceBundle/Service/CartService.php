<?php

namespace FinanceBundle\Service;

use Doctrine\ORM\EntityManager;
use FinanceBundle\Entity\User;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class CartService
{
    public const COOKIE_NAME = 'items_array';

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var array
     */
    private $items = [];

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @param RequestStack $requestStack
     */
    public function __construct(
        RequestStack $requestStack,
        EntityManager $entityManager,
        TokenStorageInterface $tokenStorage
    ) {
        $this->requestStack = $requestStack;
        $this->entityManager = $entityManager;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @return array
     */
    public function getCartItemsArray()
    {
        $this->parseCookies();

        return $this->items;
    }

    /**
     * Parse cookies and set the items array.
     */
    private function parseCookies(): void
    {
        if (!$this->items) {
            $user = $this->getUser();
            if ($user && $user->getCart() && !empty($user->getCart()['items'])) {
                foreach ($this->getUser()->getCart()['items'] as $item) {
                    $this->items[] = $item;
                }
            } else {
                $request = $this->requestStack->getCurrentRequest();
                if ($request->cookies->has(self::COOKIE_NAME)) {
                    $data = json_decode($request->cookies->get(self::COOKIE_NAME), true);
                    if (!empty($data['items'])) {
                        foreach ($data['items'] as $item) {
                            $this->items[] = $item;
                        }
                    }
                }
            }
        }
    }

    /**
     * @param int $id
     */
    public function addItem(int $id): int
    {
        $this->parseCookies();
        $this->items[] = ['id' => $id, 'isParent' => true];

        return count($this->items) - 1;
    }

    /**
     * @param int $id
     */
    public function addChildItem(int $parent, int $id, int $parentCount): void
    {
        $this->parseCookies();

        foreach ($this->items as $key => $item) {
            if ($item['isParent'] && $item['id'] == $parent) {
                if (!empty($item['options']['childs']) && count($item['options']['childs']) > 1) {
                    continue;
                } else {
                    $this->items[$key]['options']['childs'][] = [
                        'id' => $id,
                        'isParent' => false,
                        'parent' => $parent,
                        'parentCount' => $parentCount,
                        'options' => [
                            'isSpecial' => true,
                        ],
                    ];

                    return;
                }
            }
        }
    }

    /**
     * @param int $id
     */
    public function addBankItem(int $id, int $type): void
    {
        $this->parseCookies();

        $this->items[] = [
            'id' => $id,
            'isParent' => true,
            'distType' => $type,
            'options' => [
                'isBank' => true,
            ],
        ];
    }

    /**
     * @param int $id
     *
     * @return bool
     */
    public function deleteItem(int $id): bool
    {
        $this->parseCookies();
        foreach ($this->items as $key => $item) {
            if ($item['id'] == $id) {
                unset($this->items[$key]);

                return true;
            }
        }

        return false;
    }

    /**
     * @return Cookie
     */
    public function generateCookies()
    {
        $data = ['items' => $this->items];
        if ($this->getUser()) {
            $this->getUser()->setCart($data);
            $this->save($this->getUser());
        }

        return new Cookie(
            self::COOKIE_NAME,
            json_encode($data, JSON_FORCE_OBJECT),
            new \DateTime('+1 year')
        );
    }

    /**
     * @return Cookie
     */
    public function clearCart()
    {
        $this->items = [];

        if ($this->getUser()) {
            $this->getUser()->setCart([]);
            $this->save($this->getUser());
        }

        return new Cookie(
            self::COOKIE_NAME,
            json_encode(['items' => []]),
            new \DateTime('+1 year')
        );
    }

    /**
     * @return bool|User
     */
    private function getUser()
    {
        $user = $this->tokenStorage->getToken()->getUser();
        if (is_object($user)) {
            return $user;
        }

        return false;
    }

    /**
     * @param User $user
     */
    private function save(User $user)
    {
        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }
}
