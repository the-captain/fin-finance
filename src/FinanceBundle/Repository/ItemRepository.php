<?php

namespace FinanceBundle\Repository;

use FinanceBundle\Entity\Item;

class ItemRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @param $type
     *
     * @return Item|null
     */
    public function findMinCostByType($type)
    {
        $repository = $this->getEntityManager()->getRepository('FinanceBundle:Item');

        $query = $repository->createQueryBuilder('i')
            ->where('i.type = :type')
            ->setParameter('type', $type)
            ->orderBy('i.cost', 'asc')
            ->setMaxResults(1);

        $query->getQuery()->useResultCache(true, 36000)->useQueryCache(true);

        return $query->getQuery()->getOneOrNullResult();
    }

    /**
     * @return array
     */
    public function getUnpaidWithCompany()
    {
        $repository = $this->getEntityManager()->getRepository('FinanceBundle:Item');

        $query = $repository->createQueryBuilder('i')
            ->where('i.status = :status')
            ->andWhere('i.type = :type')
//            ->leftJoin('FinanceBundle\Entity\Order', 'o')
//            ->where('o.created < :date')
//            ->andWhere('o.payment = :payment')


//            ->setParameter('date', new \DateTime('-15 minutes'))
            ->setParameter('status', Item::ITEM_SOLD)
            ->setParameter('type', Item::TYPE_COMPANY);

        $query->getQuery()->useQueryCache(true);

        return $query->getQuery()->getResult();
    }
}
