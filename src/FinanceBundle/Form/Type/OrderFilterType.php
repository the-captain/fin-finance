<?php

namespace FinanceBundle\Form\Type;

use Lexik\Bundle\FormFilterBundle\Filter\Form\Type as Filters;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrderFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id', Filters\NumberFilterType::class)
            ->add('created', Filters\DateTimeFilterType::class)
            ->add('firstName', Filters\TextFilterType::class)
            ->add('lastName', Filters\TextFilterType::class)
            ->add('country', Filters\TextFilterType::class)
            ->add('city', Filters\TextFilterType::class)
            ->add('street', Filters\TextFilterType::class)
            ->add('phone', Filters\TextFilterType::class)
            ->add('zip', Filters\TextFilterType::class)
            ->add('payment', Filters\NumberFilterType::class)
            ->add(
                'status',
                Filters\ChoiceFilterType::class,
                [
                    'choices' => [
                        'Новый заказ' => 0,
                        'Оплачен' => 1,
                        'В обработке' => 2,
                        'Выполнен' => 3,
                    ],
                ]
            );
        $builder->setMethod('GET');
    }

    public function getBlockPrefix()
    {
        return null;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'allow_extra_fields' => true,
                'csrf_protection' => false,
                'validation_groups' => ['filtering'], // avoid NotBlank() constraint-related message
            ]
        );
    }
}
