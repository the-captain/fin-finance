<?php

namespace FinanceBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserAdminType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class, ['label' => 'First Name'])
            ->add('lastName', TextType::class, ['label' => 'Last Name'])
            ->add('email', EmailType::class, ['label' => 'Email', 'attr' => ['type' => 'email']])
            ->add('phone', TextType::class, ['label' => 'Phone', 'attr' => ['type' => 'numeric']])
            ->add(
                'roles',
                ChoiceType::class,
                [
                    'choices' => [
                        'Пользователь' => 'ROLE_USER',
                        'Менеджер (Доступ к заказам)' => 'ROLE_MANAGER',
                        'Редактор (Доступ к страницам и переводам)' => 'ROLE_EDITOR',
                        'Админ (Доступ ко всему)' => 'ROLE_ADMIN',
                    ],
                    'expanded' => true,
                    'multiple' => true,
                    'required' => true,
                ]
            );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => 'FinanceBundle\Entity\User']);
    }
}
