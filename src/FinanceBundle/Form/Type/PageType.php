<?php

namespace FinanceBundle\Form\Type;

use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use KMS\FroalaEditorBundle\Form\Type\FroalaEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PageType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'link',
                TextType::class,
                [
                    'label' => 'Ссылка',
                ]
            )
            ->add(
                'translations',
                TranslationsType::class,
                [
                    'required' => false,
                    'locales' => ['ru', 'en', 'es', 'de'],
                    'fields' => [
                        'title' => [
                            'label' => 'Заголовок',
                            'required' => false,
                        ],
                        'text' => [
                            'field_type' => FroalaEditorType::class,
                            'label' => 'Текст',
                        ],
                    ],
                ]
            );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'FinanceBundle\Entity\Page',
                'cascade_validation' => true,
            ]
        );
    }
}
