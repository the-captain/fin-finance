<?php

namespace FinanceBundle\Form\Type;

use FinanceBundle\Entity\Order;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class OrderType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName')
            ->add('lastName')
            ->add(
                'email',
                EmailType::class,
                [
                    'constraints' => [
                        new NotBlank(),
                        new Email(),
                    ],
                    'label' => 'Email',
                    'attr' => ['type' => 'email'],
                ]
            )
            ->add('country')
            ->add('city')
            ->add('street')
            ->add('phone', TextType::class, ['label' => 'Phone'])
            ->add('zip')
            ->add(
                'status',
                ChoiceType::class,
                [
                    'choices' => [
                        'Новый заказ' => Order::NEW_ORDER,
                        'Не оплачен' => Order::NOT_PAYED,
                        'Оплачен' => Order::PAYED,
                        'В обработке' => Order::IN_PROCESS,
                        'Выполнен' => Order::DONE,
                        'Отказ' => Order::CANCELED,
                    ],
                    'required' => true,
                ]
            )
            ->add(
                'payment',
                ChoiceType::class,
                [
                    'choices' => [
                        'PayPall' => 0,
                        'Visa/MasterCard' => 1,
                    ],
                    'required' => true,
                    'expanded' => true,
                    'multiple' => false,
                    'label' => false,
                    'data' => 1,
                ]
            );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'FinanceBundle\Entity\Order',
                'csrf_protection' => false
            ]
        );
    }
}
