<?php

namespace FinanceBundle\Form\Type;

use Lexik\Bundle\FormFilterBundle\Filter\Form\Type as Filters;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ItemFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id', Filters\NumberFilterType::class)
//            ->add('name', Filters\TextFilterType::class)
            ->add('cost', Filters\NumberFilterType::class)
            ->add('status', Filters\NumberFilterType::class)
            ->add('distantCost', Filters\NumberFilterType::class)
            ->add('country', Filters\TextFilterType::class)
//            ->add('description', Filters\TextFilterType::class)
            ->add('created', Filters\DateTimeFilterType::class);
        $builder->setMethod('GET');
    }

    public function getBlockPrefix()
    {
        return null;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'allow_extra_fields' => true,
                'csrf_protection' => false,
                'validation_groups' => ['filtering'], // avoid NotBlank() constraint-related message
            ]
        );
    }
}
