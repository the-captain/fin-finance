<?php

namespace FinanceBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class ContactType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, ['required' => true])
            ->add(
                'email',
                EmailType::class,
                [
                    'required' => true,
                    'constraints' => [
                        new NotBlank(),
                        new Email(),
                    ],
                ]
            )
            ->add('phone', TextType::class, ['required' => true])
            ->add(
                'theme',
                ChoiceType::class,
                [
                    'choices' => [
                        'PSP' => 'PSP',
                        'Готовые компании' => 'Готовые компании',
                        'Банковский счет' => 'Банковский счет',
                        'Мерчант аккаунт' => 'Мерчант аккаунт',
                        'Другое' => 'Другое',
                    ],
                    'required' => true,
                    'translation_domain' => 'FinanceBundle',
                    'choice_translation_domain' => 'messages',
                ]
            )
            ->add(
                'text',
                TextareaType::class,
                [
                    'required' => true,
                    'constraints' => [
                        new NotBlank(),
                    ],
                ]
            )
            ->add(
                'js_on',
                HiddenType::class,
                array(
                    'mapped' => false,
                    'data' => 0,
                )
            );
    }
}
