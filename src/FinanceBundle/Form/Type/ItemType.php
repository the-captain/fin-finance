<?php

namespace FinanceBundle\Form\Type;

use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use FinanceBundle\Entity\Item;
use KMS\FroalaEditorBundle\Form\Type\FroalaEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ItemType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'translations',
                TranslationsType::class,
                [
                    'required' => false,
                    'locales' => ['ru', 'en', 'es', 'de'],
                    'fields' => [
                        'name' => [
                            'label' => 'Заголовок',
                        ],
                        'description' => [
                            'field_type' => FroalaEditorType::class,
                            'label' => 'Описание',
                        ],
                    ],
                ]
            )
            ->add('cost')
            ->add('distantCost')
            ->add('charterCapital')
            ->add('country')
            ->add('created')
            ->add(
                'status',
                ChoiceType::class,
                [
                    'choices' => [
                        'Доступно для продажи' => Item::AVAILABLE,
                        'Продано' => Item::ITEM_SOLD,
                    ],
                    'required' => true,
                ]
            )
            ->add(
                'type',
                ChoiceType::class,
                [
                    'choices' => [
                        'Компания' => Item::TYPE_COMPANY,
                        'Банк' => Item::TYPE_BANK,
                        'Мерчант аккаунт' => Item::TYPE_MERCHANT,
                        'Шотландское LP' => Item::TYPE_SCOTLAND_LP_CHILD,
                        'Специальное предложение' => Item::TYPE_SPECIAL_OFFERS,
                    ],
                    'required' => true,
                ]
            );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'FinanceBundle\Entity\Item',
            ]
        );
    }
}
