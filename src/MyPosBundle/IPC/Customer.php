<?php

namespace MyPosBundle\IPC;

use FinanceBundle\Entity\OrderOperations;

/**
 * Customer details class.
 * Collect and validate client details.
 */
class Customer
{
    use OrderOperations;

    protected $email;
    protected $phone;
    protected $firstName;
    protected $lastName;
    protected $country;
    protected $city;
    protected $zip;
    protected $address;

    /**
     * Customer address.
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Customer address.
     *
     * @param string $address
     *
     * @return Customer
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Validate all set customer details.
     *
     * @return bool
     *
     * @throws IPCException
     */
    public function validate()
    {
        if ($this->getEmail() === null || !Helper::isValidEmail($this->getEmail())) {
            throw new IPCException('Invalid Email');
        }

        return true;
    }
}
