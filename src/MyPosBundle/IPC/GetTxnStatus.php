<?php

namespace MyPosBundle\IPC;

/**
 * Process IPC method: IPCGetTxnStatus.
 * Collect, validate and send API params.
 */
class GetTxnStatus extends Base
{
    private $orderID;

    public function __construct(Config $cnf)
    {
        $this->setCnf($cnf);
    }

    /**
     * Original request order id.
     *
     * @return string
     */
    public function getOrderID()
    {
        return $this->orderID;
    }

    /**
     * Original request order id.
     *
     * @param string $orderID
     *
     * @return GetTxnStatus
     */
    public function setOrderID($orderID)
    {
        $this->orderID = $orderID;

        return $this;
    }

    /**
     * Initiate API request.
     *
     * @return Response
     */
    public function process()
    {
        return $this->setPostParams('IPCGetTxnStatus');
    }
}
