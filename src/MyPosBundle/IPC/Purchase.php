<?php

namespace MyPosBundle\IPC;

/**
 * Process IPC method: IPCPurchase.
 * Collect, validate and send API params.
 */
class Purchase extends Base
{
    /**
     * @var Cart
     */
    private $cart;

    /**
     * @var Customer
     */
    private $customer;
    private $url_ok;
    private $url_cancel;
    private $url_notify;
    private $currency = 'EUR';
    private $note;
    private $orderID;

    /**
     * Return purchase object.
     *
     * @param Config $cnf
     */
    public function __construct(Config $cnf)
    {
        $this->setCnf($cnf);
    }

    /**
     * Initiate API request.
     *
     * @return bool
     */
    public function process()
    {
        $this->validate();

        $this->addPostParam('IPCmethod', 'IPCPurchase');
        $this->addPostParam('IPCVersion', $this->getCnf()->getVersion());
        $this->addPostParam('IPCLanguage', $this->getCnf()->getLang());
        $this->addPostParam('SID', $this->getCnf()->getSid());
        $this->addPostParam('WalletNumber', $this->getCnf()->getWallet());
        $this->addPostParam('KeyIndex', $this->getCnf()->getKeyIndex());
        $this->addPostParam('Source', Defines::SOURCE_PARAM);

        $this->addPostParam('Currency', $this->getCurrency());
        $this->addPostParam('Amount', $this->cart->getTotal());

        $this->addPostParam('OrderID', $this->getOrderID());
        $this->addPostParam('URL_OK', $this->getUrlOk());
        $this->addPostParam('URL_Cancel', $this->getUrlCancel());
        $this->addPostParam('URL_Notify', $this->getUrlNotify());

        $this->addPostParam('customeremail', $this->getCustomer()->getEmail());
        $this->addPostParam('customerphone', $this->getCustomer()->getPhone());
        $this->addPostParam('customerfirstnames', $this->getCustomer()->getFirstName());
        $this->addPostParam('customerfamilyname', $this->getCustomer()->getLastName());
        $this->addPostParam('customercountry', $this->getCustomer()->getCountry());
        $this->addPostParam('customercity', $this->getCustomer()->getCity());
        $this->addPostParam('customerzipcode', $this->getCustomer()->getZip());
        $this->addPostParam('customeraddress', $this->getCustomer()->getAddress());

        $this->addPostParam('Note', $this->getNote());
        $this->addPostParam('CartItems', $this->cart->getItemsCount());
        $items = $this->cart->getCart();

        $i = 1;
        foreach ($items as $v) {
            $this->addPostParam('Article_'.$i, $v['name']);
            $this->addPostParam('Quantity_'.$i, $v['quantity']);
            $this->addPostParam('Price_'.$i, $v['price']);
            $this->addPostParam('Amount_'.$i, $v['price'] * $v['quantity']);
            $this->addPostParam('Currency_'.$i, $this->getCurrency());
            ++$i;
        }

        $this->processHtmlPost();

        return true;
    }

    /**
     * Validate all set purchase details.
     *
     * @return bool
     *
     * @throws IPCException
     */
    public function validate()
    {
        if ($this->getUrlCancel() === null || !Helper::isValidURL($this->getUrlCancel())) {
            throw new IPCException('Invalid Cancel URL');
        }

        if ($this->getUrlNotify() === null || !Helper::isValidURL($this->getUrlNotify())) {
            throw new IPCException('Invalid Notify URL');
        }

        if ($this->getUrlOk() === null || !Helper::isValidURL($this->getUrlOk())) {
            throw new IPCException('Invalid Success URL');
        }

        try {
            $this->getCnf()->validate();
        } catch (\Exception $ex) {
            throw new IPCException('Invalid Config details: '.$ex->getMessage());
        }

        try {
            $this->getCart()->validate();
        } catch (\Exception $ex) {
            throw new IPCException('Invalid Cart details: '.$ex->getMessage());
        }

        try {
            $this->getCustomer()->validate();
        } catch (\Exception $ex) {
            throw new IPCException('Invalid Customer details: '.$ex->getMessage());
        }

        return true;
    }

    /**
     * Merchant Site URL where client comes after unsuccessful payment.
     *
     * @return string
     */
    public function getUrlCancel()
    {
        return $this->url_cancel;
    }

    /**
     * Merchant Site URL where client comes after unsuccessful payment.
     *
     * @param string $urlCancel
     *
     * @return Config
     */
    public function setUrlCancel($urlCancel)
    {
        $this->url_cancel = $urlCancel;

        return $this;
    }

    /**
     * Merchant Site URL where IPC posts Purchase Notify requests.
     *
     * @var string
     */
    public function getUrlNotify()
    {
        return $this->url_notify;
    }

    /**
     * Merchant Site URL where IPC posts Purchase Notify requests.
     *
     * @param string $urlNotify
     *
     * @return Config
     */
    public function setUrlNotify($urlNotify)
    {
        $this->url_notify = $urlNotify;

        return $this;
    }

    /**
     * Merchant Site URL where client comes after successful payment.
     *
     * @return string
     */
    public function getUrlOk()
    {
        return $this->url_ok;
    }

    /**
     * Merchant Site URL where client comes after successful payment.
     *
     * @param string $urlOk
     *
     * @return Config
     */
    public function setUrlOk($urlOk)
    {
        $this->url_ok = $urlOk;

        return $this;
    }

    /**
     * Cart object.
     *
     * @return Cart
     */
    public function getCart()
    {
        return $this->cart;
    }

    /**
     * Cart object.
     *
     * @param Cart $cart
     *
     * @return Purchase
     */
    public function setCart(Cart $cart)
    {
        $this->cart = $cart;

        return $this;
    }

    /**
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Customer object.
     *
     * @param Customer $customer
     *
     * @return Purchase
     */
    public function setCustomer(Customer $customer)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * ISO-4217 Three letter currency code.
     *
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * ISO-4217 Three letter currency code.
     *
     * @param string $currency
     *
     * @return Purchase
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Purchase identifier.
     *
     * @return string
     */
    public function getOrderID()
    {
        return $this->orderID;
    }

    /**
     * Purchase identifier - must be unique.
     *
     * @param string $orderID
     *
     * @return Purchase
     */
    public function setOrderID($orderID)
    {
        $this->orderID = $orderID;

        return $this;
    }

    /**
     * Optional note to purchase.
     *
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Optional note to purchase.
     *
     * @param string $note
     *
     * @return Purchase
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }
}
