<?php

namespace MyPosBundle\IPC;

/**
 * Process IPC method: IPCRefund.
 * Collect, validate and send API params.
 */
class Refund extends Base
{
    private $currency = 'EUR';
    private $amount;
    private $trnref;
    private $orderID;

    /**
     * Return Refund object.
     *
     * @param Config $cnf
     */
    public function __construct(Config $cnf)
    {
        $this->setCnf($cnf);
    }

    /**
     * Initiate API request.
     *
     * @return bool
     */
    public function process()
    {
        $this->validate();

        $this->addPostParam('IPCmethod', 'IPCRefund');
        $this->addPostParam('IPCVersion', $this->getCnf()->getVersion());
        $this->addPostParam('IPCLanguage', $this->getCnf()->getLang());
        $this->addPostParam('SID', $this->getCnf()->getSid());
        $this->addPostParam('WalletNumber', $this->getCnf()->getWallet());
        $this->addPostParam('KeyIndex', $this->getCnf()->getKeyIndex());
        $this->addPostParam('Source', Defines::SOURCE_PARAM);

        $this->addPostParam('Currency', $this->getCurrency());
        $this->addPostParam('Amount', $this->getAmount());

        $this->addPostParam('OrderID', $this->getOrderID());
        $this->addPostParam('IPC_Trnref', $this->getTrnref());
        $this->addPostParam('OutputFormat', $this->getOutputFormat());

        $response = $this->processPost()->getData(CASE_LOWER);
        if (
            (empty($response['ipc_trnref']) || $response['ipc_trnref'] != $this->getTrnref()) ||
            (empty($response['amount']) || $response['amount'] != $this->getAmount()) ||
            (empty($response['currency']) || $response['currency'] != $this->getCurrency()) ||
            $response['status'] != \Satabank\IPC\Defines::STATUS_SUCCESS
        ) {
            return false;
        }

        return true;
    }

    /**
     * Validate all set refund details.
     *
     * @return bool
     *
     * @throws IPCException
     */
    public function validate()
    {
        try {
            $this->getCnf()->validate();
        } catch (\Exception $ex) {
            throw new IPCException('Invalid Config details: '.$ex->getMessage());
        }

        if ($this->getAmount() === null || !Helper::isValidAmount($this->getAmount())) {
            throw new IPCException('Invalid Amount');
        }

        if ($this->getCurrency() === null || !Helper::isValidCurrency($this->getCurrency())) {
            throw new IPCException('Invalid Currency');
        }

        if ($this->getTrnref() === null || !Helper::isValidTrnRef($this->getTrnref())) {
            throw new IPCException('Invalid TrnRef');
        }

        if ($this->getOrderID() === null || !Helper::isValidOrderId($this->getOrderID())) {
            throw new IPCException('Invalid OrderId');
        }

        if ($this->getOutputFormat() === null || !Helper::isValidOutputFormat($this->getOutputFormat())) {
            throw new IPCException('Invalid Output format');
        }

        return true;
    }

    /**
     * Refund amount.
     *
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Refund amount.
     *
     * @param float $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * ISO-4217 Three letter currency code.
     *
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * ISO-4217 Three letter currency code.
     *
     * @param string $currency
     *
     * @return Refund
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Transaction reference - transaction unique identifier.
     *
     * @return string
     */
    public function getTrnref()
    {
        return $this->trnref;
    }

    /**
     * Transaction reference - transaction unique identifier.
     *
     * @param string $trnref
     *
     * @return Refund
     */
    public function setTrnref($trnref)
    {
        $this->trnref = $trnref;

        return $this;
    }

    /**
     * Request identifier - must be unique.
     *
     * @return string
     */
    public function getOrderID()
    {
        return $this->orderID;
    }

    /**
     * Request identifier - must be unique.
     *
     * @param string $orderID
     *
     * @return Refund
     */
    public function setOrderID($orderID)
    {
        $this->orderID = $orderID;

        return $this;
    }
}
