<?php

namespace MyPosBundle\IPC;

/**
 * Process IPC method: IPCGetTxnLog.
 * Collect, validate and send API params.
 */
class IPCGetTxnLog extends Base
{
    private $orderID;

    /**
     * Return IPCGetTxnLog object.
     *
     * @param Config $cnf
     */
    public function __construct(Config $cnf)
    {
        $this->setCnf($cnf);
    }

    /**
     * Original request order id.
     *
     * @return string
     */
    public function getOrderID()
    {
        return $this->orderID;
    }

    /**
     * Original request order id.
     *
     * @param string $orderID
     *
     * @return IPCGetTxnLog
     */
    public function setOrderID($orderID)
    {
        $this->orderID = $orderID;

        return $this;
    }

    /**
     * Initiate API request.
     *
     * @return Response
     */
    public function process()
    {
        return $this->setPostParams('IPCGetTxnLog');
    }
}
