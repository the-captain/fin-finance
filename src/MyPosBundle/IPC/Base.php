<?php

namespace MyPosBundle\IPC;

/**
 * Base API Class. Contains basic API-connection methods.
 */
class Base
{
    /**
     * @var string Output format from API for some requests may be XML or JSON
     */
    protected $outputFormat = Defines::COMMUNICATION_FORMAT_JSON;
    /**
     * @var Config
     */
    private $cnf;
    /**
     * @var array Params for API Request
     */
    private $params = array();

    /**
     * Verify signature of API Request params against the API public key.
     *
     * @param string $data      Signed data
     * @param string $signature Signature in base64 format
     * @param string $pubKey    API public key
     *
     * @return bool
     */
    public static function isValidSignature($data, $signature, $pubKey)
    {
        $pubKeyId = openssl_get_publickey($pubKey);
        $res = openssl_verify($data, base64_decode($signature), $pubKeyId, Defines::SIGNATURE_ALGO);
        openssl_free_key($pubKeyId);
        if ($res != 1) {
            return false;
        }

        return true;
    }

    /**
     * Generate HTML form with POST params and auto-submit it.
     */
    protected function processHtmlPost()
    {
        //Add request signature
        $this->params['Signature'] = $this->createSignature();

        $c = '';
        $c = '<body onload="document.ipcForm.submit();">';
        $c .= '<form id="ipcForm" name="ipcForm" action="'.$this->getCnf()->getIpcURL().'" method="post">';
        foreach ($this->params as $k => $v) {
            $c .= '<input type="hidden" name="'.$k.'" value="'.$v."\"  />\n";
        }
        $c .= '</form></body>';
        echo $c;
        exit;
    }

    /**
     * Create signature of API Request params against the SID private key.
     *
     * @return strung base64 encoded signature
     */
    private function createSignature()
    {
        $params = $this->params;
        foreach ($params as $k => $v) {
            $params[$k] = Helper::unescape($v);
        }
        $concData = base64_encode(implode('-', $params));
        $privKey = openssl_get_privatekey($this->getCnf()->getPrivateKey());
        openssl_sign($concData, $signature, $privKey, Defines::SIGNATURE_ALGO);

        return base64_encode($signature);
    }

    /**
     * Return IPC\Config object with current IPC configuration.
     *
     * @return Config
     */
    protected function getCnf()
    {
        return $this->cnf;
    }

    /**
     * Set Config object with current IPC configuration.
     *
     * @param Config $cnf
     */
    protected function setCnf(Config $cnf)
    {
        $this->cnf = $cnf;
    }

    /**
     * @param $ipcMethod
     *
     * @return Response
     */
    protected function setPostParams($ipcMethod)
    {
        $this->validate();

        $this->addPostParam('IPCmethod', $ipcMethod);
        $this->addPostParam('IPCVersion', $this->getCnf()->getVersion());
        $this->addPostParam('IPCLanguage', $this->getCnf()->getLang());
        $this->addPostParam('SID', $this->getCnf()->getSid());
        $this->addPostParam('WalletNumber', $this->getCnf()->getWallet());
        $this->addPostParam('KeyIndex', $this->getCnf()->getKeyIndex());
        $this->addPostParam('Source', Defines::SOURCE_PARAM);
        $this->addPostParam('OrderID', $this->getOrderID());
        $this->addPostParam('OutputFormat', $this->getOutputFormat());

        return $this->processPost();
    }

    /**
     * Validate all set details.
     *
     * @return bool
     *
     * @throws IPCException
     */
    public function validate()
    {
        try {
            $this->getCnf()->validate();
        } catch (\Exception $ex) {
            throw new IPCException('Invalid Config details: '.$ex->getMessage());
        }

        if ($this->getOrderID() === null || !Helper::isValidOrderId($this->getOrderID())) {
            throw new IPCException('Invalid OrderId');
        }

        if ($this->getOutputFormat() === null || !Helper::isValidOutputFormat($this->getOutputFormat())) {
            throw new IPCException('Invalid Output format');
        }

        return true;
    }

    /**
     * Return current set output format for API Requests.
     *
     * @return string
     */
    public function getOutputFormat()
    {
        return $this->outputFormat;
    }

    /**
     * Set current set output format for API Requests.
     *
     * @param string $outputFormat
     */
    public function setOutputFormat($outputFormat)
    {
        $this->outputFormat = $outputFormat;
    }

    /**
     * Add API request param.
     *
     * @param string $paramName
     * @param string $paramValue
     */
    protected function addPostParam($paramName, $paramValue)
    {
        $this->params[$paramName] = Helper::escape(Helper::unescape($paramValue));
    }

    /**
     * Send POST Request to API and returns Response object with validated response data.
     *
     * @return Response
     *
     * @throws IPCException
     */
    protected function processPost()
    {
        $this->params['Signature'] = $this->createSignature();
        $cont = array();
        $url = parse_url($this->getCnf()->getIpcURL());
        if (!isset($url['port'])) {
            switch ($url['scheme']) {
                case 'https':
                    $url['port'] = 443;
                    $ssl = 'ssl://';
                    break;
                case 'http':
                    $url['port'] = 80;
                    $ssl = '';
                    break;
            }
        }
        $postData = http_build_query($this->params);
        $fp = @fsockopen($ssl.$url['host'], $url['port'], $errno, $errstr, 10);
        if (!$fp) {
            throw new IPCException('Error connectiong IPC URL');
        } else {
            $eol = "\r\n";
            $path = $url['path'].(!(empty($url['query'])) ? ('?'.$url['query']) : '');
            fwrite($fp, 'POST '.$path.' HTTP/1.1'.$eol);
            fwrite($fp, 'Host: '.$url['host'].$eol);
            fwrite($fp, 'Content-type: application/x-www-form-urlencoded'.$eol);
            fwrite($fp, 'Content-length: '.strlen($postData).$eol);
            fwrite($fp, 'Connection: close'.$eol.$eol);
            fwrite($fp, $postData.$eol.$eol);

            try {
                $result = '';
                while (!feof($fp)) {
                    $result .= fgets($fp, 1024);
                }
                fclose($fp);
                $result = explode($eol.$eol, $result, 2);
                $header = isset($result[0]) ? $result[0] : '';
                $cont = isset($result[1]) ? $result[1] : '';

                //Проверявам за Transfer-Encoding: chunked
                if (!empty($cont) && strpos($header, 'Transfer-Encoding: chunked') !== false) {
                    $check = $this->httpChunkedDecode($cont);
                    if ($check) {
                        $cont = $check;
                    }
                }
                if ($cont) {
                    $cont = trim($cont);
                }
            } catch (\Exception $exception) {
                throw new IPCException('Error connectiong IPC URL');
            }

            return Response::getInstance($this->getCnf(), $cont, $this->outputFormat);
        }
    }

    /**
     * Alternative of php http-chunked-decode function.
     *
     * @param string $chunk
     *
     * @return mixed
     */
    private function httpChunkedDecode($chunk)
    {
        $pos = 0;
        $len = strlen($chunk);
        $dechunk = null;

        while (($pos < $len) && ($chunkLenHex = substr(
                $chunk,
                $pos,
                ($newlineAt = strpos($chunk, "\n", $pos + 1)) - $pos
            ))) {
            if (!$this->isHex($chunkLenHex)) {
                return false;
            }

            $pos = $newlineAt + 1;
            $chunkLen = hexdec(rtrim($chunkLenHex, "\r\n"));
            $dechunk .= substr($chunk, $pos, $chunkLen);
            $pos = strpos($chunk, "\n", $pos + $chunkLen) + 1;
        }

        return $dechunk;
    }

    /**
     * determine if a string can represent a number in hexadecimal.
     *
     * @param string $hex
     *
     * @return bool true if the string is a hex, otherwise false
     */
    private function isHex($hex)
    {
        // regex is for weenies
        $hex = strtolower(trim(ltrim($hex, '0')));
        if (empty($hex)) {
            $hex = 0;
        }
        $dec = hexdec($hex);

        return $hex == dechex($dec);
    }
}
