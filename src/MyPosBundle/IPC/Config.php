<?php

namespace MyPosBundle\IPC;

/**
 * IPC Configuration class.
 */
class Config
{
    private $privateKey = null;
    private $APIPublicKey = null;
    private $keyIndex = null;
    private $sid;
    private $wallet;
    private $lang = 'en';
    private $version = '1.0';
    private $ipc_url = 'https://www.mypos.eu/vmp/checkout';
    private $developerKey;

    /**
     * Store private RSA key as a filepath.
     *
     * @param string $path File path
     *
     * @return Config
     *
     * @throws IPCException
     */
    public function setPrivateKeyPath($path)
    {
        if (!is_file($path) || !is_readable($path)) {
            throw new IPCException('Private key not found in:'.$path);
        }
        $this->privateKey = file_get_contents($path);

        return $this;
    }

    /**
     * IPC API public RSA key.
     *
     * @return string
     */
    public function getAPIPublicKey()
    {
        return $this->APIPublicKey;
    }

    /**
     * IPC API public RSA key.
     *
     * @param string $publicKey
     *
     * @return Config
     */
    public function setAPIPublicKey($publicKey)
    {
        $this->APIPublicKey = $publicKey;

        return $this;
    }

    /**
     * IPC API public RSA key as a filepath.
     *
     * @param string $path
     *
     * @return Config
     *
     * @throws IPCException
     */
    public function setAPIPublicKeyPath($path)
    {
        if (!is_file($path) || !is_readable($path)) {
            throw new IPCException('Public key not found in:'.$path);
        }
        $this->APIPublicKey = file_get_contents($path);

        return $this;
    }

    /**
     * Language code (ISO 639-1).
     *
     * @return string
     */
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * Language code (ISO 639-1).
     *
     * @param string $lang
     *
     * @return Config
     */
    public function setLang($lang)
    {
        $this->lang = $lang;

        return $this;
    }

    /**
     * Store private RSA key.
     *
     * @return type
     */
    public function getDeveloperKey()
    {
        return $this->developerKey;
    }

    /**
     * Set myPOS developer key.
     *
     * @param string $developerKey
     *
     * @return Config
     */
    public function setDeveloperKey($developerKey)
    {
        $this->developerKey = $developerKey;

        return $this;
    }

    /**
     * Validate all set config details.
     *
     * @return bool
     *
     * @throws IPCException
     */
    public function validate()
    {
        if ($this->getKeyIndex() === null || !is_numeric($this->getKeyIndex())) {
            throw new IPCException('Invalid Key Index');
        }

        if ($this->getIpcURL() === null || !Helper::isValidURL($this->getIpcURL())) {
            throw new IPCException('Invalid IPC URL');
        }

        if ($this->getSid() === null || !is_numeric($this->getSid())) {
            throw new IPCException('Invalid SID');
        }

        if ($this->getWallet() === null || !is_numeric($this->getWallet())) {
            throw new IPCException('Invalid Wallet number');
        }

        if ($this->getVersion() === null) {
            throw new IPCException('Invalid IPC Version');
        }

        if (!openssl_get_privatekey($this->getPrivateKey())) {
            throw new IPCException('Invalid Private key');
        }

        return true;
    }

    /**
     *  Keyindex used for signing request.
     *
     * @return type
     */
    public function getKeyIndex()
    {
        return $this->keyIndex;
    }

    /**
     * Keyindex used for signing request.
     *
     * @param int $keyIndex
     *
     * @return Config
     */
    public function setKeyIndex($keyIndex)
    {
        $this->keyIndex = $keyIndex;

        return $this;
    }

    /**
     * IPC API URL.
     *
     * @return string
     */
    public function getIpcURL()
    {
        return $this->ipc_url;
    }

    /**
     * IPC API URL.
     *
     * @param string $ipc_url
     *
     * @return Config
     */
    public function setIpcURL($ipc_url)
    {
        $this->ipc_url = $ipc_url;

        return $this;
    }

    /**
     * Store ID.
     *
     * @return int
     */
    public function getSid()
    {
        return $this->sid;
    }

    /**
     * Store ID.
     *
     * @param int $sid
     *
     * @return Config
     */
    public function setSid($sid)
    {
        $this->sid = $sid;

        return $this;
    }

    /**
     * Wallet number.
     *
     * @return string
     */
    public function getWallet()
    {
        return $this->wallet;
    }

    /**
     * Wallet number.
     *
     * @param string $wallet
     *
     * @return Config
     */
    public function setWallet($wallet)
    {
        $this->wallet = $wallet;

        return $this;
    }

    /**
     * API Version.
     *
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * API Version.
     *
     * @param string $version
     *
     * @return Config
     */
    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * Store private RSA key.
     *
     * @return type
     */
    public function getPrivateKey()
    {
        return $this->privateKey;
    }

    /**
     * Store private RSA key.
     *
     * @param string $privateKey
     *
     * @return Config
     */
    public function setPrivateKey($privateKey)
    {
        $this->privateKey = $privateKey;

        return $this;
    }
}
