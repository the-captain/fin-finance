<?php

namespace MyPosBundle\IPC;

/**
 * IPC Response class. Parse and validate income data.
 */
class Response
{
    /**
     * @var Config;
     */
    private $cnf;
    private $raw_data;
    private $format;
    private $data;
    private $signature;

    /**
     * @param Config       $cnf
     * @param string|array $raw_data
     * @param string       $format   COMMUNICATION_FORMAT_JSON|COMMUNICATION_FORMAT_XML|COMMUNICATION_FORMAT_POST
     */
    public function __construct(Config $cnf, $raw_data, $format)
    {
        $this->cnf = $cnf;
        $this->setData($raw_data, $format);
    }

    private function setData($raw_data, $format)
    {
        if (empty($raw_data)) {
            throw new IPCException('Invalid Reponce data');
        }

        $this->format = $format;
        $this->raw_data = $raw_data;

        switch ($this->format) {
            case Defines::COMMUNICATION_FORMAT_JSON:
                $this->data = json_decode($this->raw_data, 1);
                break;
            case Defines::COMMUNICATION_FORMAT_XML:
                $this->data = (array) new \SimpleXMLElement($this->raw_data);
                break;
            case Defines::COMMUNICATION_FORMAT_POST:
                $this->data = $this->raw_data;
                break;
            default:
                throw new IPCException('Invalid response format!');
                break;
        }

        if (empty($this->data)) {
            throw new IPCException('No IPC Response!');
        }

        $this->exctractSignature() && $this->verifySignature();

        return $this;
    }

    private function exctractSignature()
    {
        if (is_array($this->data) && !empty($this->data)) {
            foreach ($this->data as $k => $v) {
                if (strtolower($k) == 'signature') {
                    $this->signature = $v;
                    unset($this->data[$k]);
                }
            }
        }

        return true;
    }

    private function verifySignature()
    {
        if (empty($this->signature)) {
            throw new IPCException('Missing request signature!');
        }

        if (!$this->cnf) {
            throw new IPCException('Missing config object!');
        }

        $pubKeyId = openssl_get_publickey($this->cnf->getAPIPublicKey());
        if (!openssl_verify(
            $this->getSignData(),
            base64_decode($this->signature),
            $pubKeyId,
            Defines::SIGNATURE_ALGO
        )
        ) {
            throw new IPCException('Signature check failed!');
        }
    }

    private function getSignData()
    {
        return base64_encode(implode('-', Helper::getValuesFromMultiDemensionalArray($this->data)));
    }

    /**
     * Static class to create Response object.
     *
     * @param string|array $raw_data
     * @param type         $format
     *
     * @return \self
     */
    public static function getInstance(Config $cnf, $raw_data, $format)
    {
        return new self($cnf, $raw_data, $format);
    }

    /**
     * Validate Signature param from IPC repsonce.
     *
     * @return bool
     */
    public function isSignatureCorrect()
    {
        try {
            $this->verifySignature();
        } catch (\Exception $ex) {
            return false;
        }

        return true;
    }

    /**
     * Request param: Signature.
     *
     * @return string
     */
    public function getSignature()
    {
        return $this->signature;
    }

    //############################################

    /**
     * Request param: Status.
     *
     * @return int
     */
    public function getStatus()
    {
        return Helper::getArrayVal($this->getData(CASE_LOWER), 'status');
    }

    /**
     * Return IPC Response in array.
     *
     * @param int $case CASE_LOWER|CASE_UPPER
     *
     * @return array
     *
     * @throws IPCException
     */
    public function getData($case = null)
    {
        if ($case !== null) {
            if (!in_array($case, array(CASE_LOWER, CASE_UPPER))) {
                throw new IPCException('Invalid Key Case!');
            }

            return array_change_key_case($this->data, $case);
        }

        return $this->data;
    }

    /**
     * Request param: StatusMsg.
     *
     * @return string
     */
    public function getStatusMsg()
    {
        return Helper::getArrayVal($this->getData(CASE_LOWER), 'statusmsg');
    }

    /**
     * Return IPC Response in original format json/xml/array.
     *
     * @return string|array
     */
    public function getDataRaw()
    {
        return $this->raw_data;
    }
}
