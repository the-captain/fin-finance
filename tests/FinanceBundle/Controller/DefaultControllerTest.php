<?php

namespace Tests\FinanceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    public function testIndexAction()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/en/');

        $this->assertTrue($client->getResponse()->isSuccessful());

        $this->assertContains('Payment Institutions', $crawler->filter('.content h1')->text());

    }
}
