<?php

namespace Tests\FinanceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ItemControllerTest extends WebTestCase
{

    public function testMerchantAccountPageAction()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/en/content/our-services/merchant-account.html');

        $this->assertTrue($client->getResponse()->isSuccessful());

        $this->assertContains(
            'Turnkey merchant account from the market experts',
            $crawler->filter('.content h1')->text()
        );
    }

    public function testReadyCompaniesPageAction()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/en/content/our-services/offshore-companies.html');

        $this->assertTrue($client->getResponse()->isSuccessful());

        $this->assertContains('Ready-made companies for your business', $crawler->filter('.content h1')->text());
    }

    public function testBankAccountPageAction()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/en/content/our-services/bank-account.html');

        $this->assertTrue($client->getResponse()->isSuccessful());

        $this->assertContains(
            'Bank account in Europe for legal entities and individuals',
            $crawler->filter('.content h1')->text()
        );
    }


    public function testSpecialOffersPageAction()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/en/content/special-offers.html');

        $this->assertTrue($client->getResponse()->isSuccessful());

        $this->assertContains('Special offers', $crawler->filter('.content h1')->text());
    }
}