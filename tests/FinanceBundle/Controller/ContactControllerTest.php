<?php

namespace Tests\FinanceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ContactControllerTest extends WebTestCase
{

    public function testIndexPageAction()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/en/content/contacts.html');

        $this->assertTrue($client->getResponse()->isSuccessful());

        $this->assertContains('Contacts', $crawler->filter('.content h1')->text());
    }

}
