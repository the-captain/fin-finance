<?php

namespace Tests\FinanceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PageControllerTest extends WebTestCase
{
    public function testPageAction()
    {

        $client = static::createClient();

        $crawler = $client->request('GET', '/en/content/our-services/card-issue.html');

        $this->assertTrue($client->getResponse()->isSuccessful());

        $this->assertContains(
            'Visa and MasterCard bank cards: assistance with issuing',
            $crawler->filter('.content h1')->text()
        );
    }
}
